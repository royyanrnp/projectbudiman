<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldBankInfoNpwpOnUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->Integer('id_bank_fk')->nullable();
            $table->string('bank_acc_num')->nullable();
            $table->string('bank_acc_name')->nullable();
            $table->string('no_npwp')->nullable()->unique();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('gender')->nullable();
            $table->integer('kodepos')->nullable();
            $table->string('status_perkawinan')->nullable();
            $table->integer('refferal')->nullable();
            $table->boolean('disabled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('id_bank_fk');
            $table->dropColumn('bank_acc_num');
            $table->dropColumn('bank_acc_name');
            $table->dropColumn('no_npwp');
            $table->dropColumn('tempat_lahir');
            $table->dropColumn('tanggal_lahir');
            $table->dropColumn('gender');
            $table->dropColumn('kodepos');
            $table->dropColumn('status_perkawinan');
            $table->dropColumn('refferal');
            $table->dropColumn('disabled');
        });
    }
}
