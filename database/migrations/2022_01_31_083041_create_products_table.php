<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->tinyInteger('category_product');
            $table->tinyInteger('type_product');
            $table->integer('price');
            $table->integer('gen_1');
            $table->integer('gen_2');
            $table->integer('gen_3');
            $table->text('desc');
            $table->boolean('is_register')->default(false);
            $table->boolean('status')->default(false);
            $table->tinyInteger('flag')->default(0);
            $table->tinyInteger('index')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
