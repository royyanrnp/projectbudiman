<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid');
            $table->integer('id_upline_fk')->nullable();
            $table->integer('id_sponsor_fk')->nullable();
            $table->string('username')->unique();
            $table->string('name');
            $table->string('no_ktp')->nullable()->unique();
            $table->string('handphone');
            $table->string('email')->unique();
            $table->text('address');
            $table->string('id_province');
            $table->string('id_city');
            $table->string('id_district');
            $table->string('id_village');
            // $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('flag')->default(0);
            $table->boolean('status')->default(false);
            $table->date('activated_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
