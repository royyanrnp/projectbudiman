<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->Integer('id_trans_fk');
            $table->Integer('id_barang_fk');
            $table->String('name');
            $table->Integer('qty');
            $table->Integer('base_price')->nullable();
            $table->Integer('sell_price')->nullable();
            $table->Integer('pv')->default(0);
            $table->Integer('bv')->default(0);
            $table->decimal('weight_kg')->default(0);
            $table->Integer('cashback')->nullable();
            $table->softDeletes('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
