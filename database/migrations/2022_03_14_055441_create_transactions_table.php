<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cust_fk');
            $table->string('code_trans');
            $table->integer('shipping_cost')->default(0);
            $table->string('shipping_name');
            $table->string('shipping_phone');
            $table->string('shipping_address')->nullable();
            $table->Integer('shipping_village')->nullable();
            $table->Integer('shipping_district')->nullable();
            $table->Integer('shipping_city')->nullable();
            $table->Integer('shipping_province')->nullable();
            $table->Integer('purchase_cost')->default(0);
            $table->string('status');
            $table->date('transaction_date');
            $table->softDeletes('deleted_at')->nullable();
            $table->Integer('created_userid')->nullable();
            $table->Integer('updated_userid')->nullable();
            $table->Integer('pv_total')->default(0);
            $table->Integer('bv_total')->default(0);
            $table->Integer('is_pickup')->nullable();
            $table->Integer('subsidi_shipping')->default(0);
            $table->Integer('courier');
            $table->Integer('approved_by')->nullable();
            $table->date('shipping_date')->nullable();
            $table->String('shipping_by')->nullable();
            $table->String('pickup_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
