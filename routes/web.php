<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TypeProductController;
use App\Http\Controllers\CategoryProductController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserLoginController;
use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TransactionDetailController;
use App\Http\Controllers\TransactionListController;
use App\Http\Controllers\CourierController;
use App\Http\Controllers\ProductDetailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/product-detail', function(){
//     return view('product_detail/index');
// });

Route::get('/registration', function () {
    return view('register/index');
});

// Route::get('/', function () {
//     return view('landingpage/index');
// });

Route::resource('/', LandingPageController::class);

// Route::get('/edit-user',function(){
//     return view('edit_user/index');
// });

// Route::get('/profile', function(){
//     return view('profile_user/index');
// });

// Route::get('/kurir', function(){
//     return view('kurir/index');
// });

// Route::get('/transaction-detail', function(){
//     return view('transaction_detail/index');
// });

// Route::get('/transaction-list', function(){
//     return view('transaction_list/index');
// });

Route::post('search_user', [UserController::class, 'search_user']);
Route::resource('user-admin', UserController::class);
// Route::get('/user-admin', function () {
//     return view('admin/user/index');
// });

// Route::get('/cart',function(){
//     return view('cart/index');
// });

Route::get('/dashboard', function(){
    return view('dashboard/index');
});

Auth::routes();

Route::get('profile/{id}', [UserController::class, 'profile']);
Route::post('reset_password_user', [UserController::class, 'reset_password_user']);
Route::post('reset_password_admin/{uid}', [UserController::class, 'reset_password_admin']);
Route::post('edit_profile/{id}', [UserController::class, 'edit_profile']);
Route::post('edit_address/{id}', [UserController::class, 'edit_address']);
Route::post('edit_bank/{id}', [UserController::class, 'edit_bank']);
Route::post('profile_picture/{id}', [UserController::class, 'profile_picture']);
Route::get('/home',[HomeController::class,'index'])->name('home');

Route::resource('registration', RegisterController::class);
Route::resource('type', TypeProductController::class);
Route::resource('category', CategoryProductController::class);
Route::get('product/add',[ProductController::class,'add_product']);
Route::post('product/search',[ProductController::class,'search']);
Route::resource('product', ProductController::class);



// Route::resource('product-detail', ProductDetailController::class);

Route::get('city/{id}', [HelperController::class,'cityByProvince']);
Route::get('district/{id}', [HelperController::class,'districtByCity']);
Route::get('village/{id}', [HelperController::class,'villageByDistrict']);

// Route::get('/login', function(){
//     return view('auth/login.blade');
// });

Route::resource('userlogin', UserLoginController::class);
// Route::get('/login', function(){
//     return view('auth/login.blade');
// });
Route::get('min_cart/{id}', [CartController::class, 'min_product']);
Route::get('add_cart/{id}', [CartController::class, 'add_product']);
Route::get('cart_delete/{id}', [CartController::class, 'destroy']);
Route::resource('cart', CartController::class);

// test


Route::resource('kurir', CourierController::class);
Route::post('settlement/{id}', [TransactionController::class, 'settlement']);

Route::post('transaction-payment_confirm/{code_trans}', [TransactionController::class, 'payment_confirm']);
Route::get('transaction_detail/{code_trans}', [TransactionController::class, 'detail']);
Route::resource('transaction', TransactionController::class);
// Route::resource('transaction_detail', TransactionDetailController::class);


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::resource('transaction-list', TransactionListController::class);
// Route::get('/home', HomeController::class)->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
