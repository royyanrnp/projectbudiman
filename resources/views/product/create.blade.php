@extends('layouts.master-admin')

@section('content')
    <div class="container-fluid mt-3">
        <div class="card curved-10">
            <div class="card-body">
                <h4 class="mb-5">Add Product</h4>
                <form action="{{url('product')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control gray" id="name" name="name" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Category Product</label>
                                <select class="form-control gray" name="category_product" id="category_product">
                                    <option value="" disabled>Category Select</option>
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Type Product</label>
                                <select class="form-control gray" name="type_product" id="type_product">
                                    <option value="" disabled>Category Select</option>
                                    @foreach ($types as $type)
                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Price</label>
                                <input type="number" min="0" class="form-control gray" id="price" name="price" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Registration Product?</label>
                                <select class="form-control gray" name="is_register" id="is_register">
                                    <option value="" disabled>Yes Or No</option>
                                    <option value="false">No</option>
                                    <option value="true">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Status Product</label>
                                <select class="form-control gray" name="status" id="status">
                                    <option value="" disabled>Actived Or Deactivatec</option>
                                    <option value="false">Deactivated</option>
                                    <option value="true">Activated</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">gen 1</label>
                                <input type="number" min="0" class="form-control gray" id="gen_1" name="gen_1" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">gen 2</label>
                                <input type="number" min="0" class="form-control gray" id="gen_2" name="gen_2" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">gen 3</label>
                                <input type="number" min="0" class="form-control gray" id="gen_3" name="gen_3" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="desc" id="desc" cols="30" rows="3" class="form-control gray"
                                    required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="url_image">Upload Image</label>
                                <input type="file" name="url_image" id="url_image" class="form-control-file">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end mt-3">
                        <div class="col-md-2">
                            <a href="{{url('product')}}" class="btn btn-block btn-dark curved-20">Cancel</a>
                        </div>
                
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection
