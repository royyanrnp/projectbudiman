@extends('layouts.master-admin')

@section('content')
<div class="container-fluid mt-3">
    <div class="card curved-10">
        <div class="card-body">

            <div class="row justify-content-between mb-3">
                <div class="col-md-3">
                    <h4>Product</h4>
                </div>

                <div class="col-md-2">
                    <a href="{{url('product/add')}}" class="btn btn-block btn-lg btn-dark text-small">
                        Add Product
                    </a>
                </div>
            </div>

            <section class="table-user">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tableWithSearch">
                        <thead class="">
                            <tr>
                                {{-- <th scope="col">#</th> --}}
                                <th scope="col">Product Code</th>
                                <th scope="col">Name Product</th>
                                <th scope="col">Category Product</th>
                                <th scope="col">Type Product</th>
                                <th scope="col">Price</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{$product->code}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->category->name}}</td>
                                <td>{{$product->type->name}}</td>
                                <td>{{$product->price}}</td>

                                <td>

                                    <button class="btn btn-white text-primary" data-toggle="modal"
                                        data-target="#edit_product{{$product->id}}" data-toggle="tooltip"
                                        data-placement="bottom" title="Edit Product">
                                        <i class="fas fa-edit"></i>
                                    </button>


                                    <button class="btn btn-white text-primary" data-toggle="modal"
                                        data-target="#delete_product{{$product->id}}" data-toggle="tooltip"
                                        data-placement="bottom" title="Delete Product">
                                        <i class="fas fa-trash"></i>
                                    </button>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>

            @foreach ($products as $product)
            <form action="{{url('product/'.$product->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <!-- Modal Edit -->
                <div class="modal fade" id="edit_product{{$product->id}}" tabindex="-1"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content curved-20">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Product </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body container">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label>Name</label>
                                            <input class="form-control gray" type="text" name="name" id="name"
                                                value="{{$product->name}}">
                                        </div>
                                        <div class="mb-3">
                                            <label>Category Product</label>
                                            <select class="form-control gray" name="category_product"
                                                id="category_product">
                                                @foreach ($categories as $category)
                                                <option value="{{$category->id}}"
                                                    {{$product->category_product = $category->id ? 'selected' : ''}}>
                                                    {{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label>Type Product</label>
                                            <label for="">Type Product</label>
                                            <select class="form-control gray" name="type_product" id="type_product">
                                                <option value="{{$product->type_product}}" selected>
                                                    {{$product->type->name}}</option>
                                                @foreach ($types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input class="form-control" type="text" name="name" id="name" value="{{$product->name}}">
                                            --}}
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-group">
                                                <label for="">Price</label>
                                                <input type="number" min="0" class="form-control gray" id="price"
                                                    name="price" value="{{$product->price}}">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-group">
                                                <label for="">Status Product</label>
                                                <select class="form-control gray" name="status" id="status">
                                                    <option value="true" {{$product->status == TRUE ? 'selected' : ''}}>
                                                        Activated</option>
                                                    <option value="false"
                                                        {{$product->status == FALSE ? 'selected' : ''}}>Deactivated
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <div class="form-group">
                                                <label for="">gen 1</label>
                                                <input type="number" min="0" class="form-control gray" id="gen_1"
                                                    name="gen_1" value="{{$product->gen_1}}">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-group">
                                                <label for="">gen 2</label>
                                                <input type="number" min="0" class="form-control gray" id="gen_2"
                                                    name="gen_2" value="{{$product->gen_2}}">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-group">
                                                <label for="">gen 3</label>
                                                <input type="number" min="0" class="form-control gray" id="gen_3"
                                                    name="gen_3" value="{{$product->gen_3}}">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="">Description</label>
                                            <textarea name="desc" id="desc" cols="10" rows="5"
                                                class="form-control gray">{{$product->desc}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Image Product </label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{asset($product->media->url_image ?? '')}}" width="300px">
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Upload Image</label>
                                                    <input type="file" name="url_image" id="url_image"
                                                        class="form-control-file">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>

            <!-- Modal Delete -->
            <div class="modal fade" id="delete_product{{$product->id}}" tabindex="-1"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content curved-20">
                        <div class="modal-header border-0">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4> Are you sure want to delete this Product</h4><br>
                            <h5>{{$product->name}}</h5>
                            <div class="row justify-content-end mt-3">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-dark curved-20"
                                        data-dismiss="modal">Close</button>
                                </div>

                                <div class="col-md-3">
                                    <form action="{{url('product/'.$product->id)}}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-danger curved-20">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>


</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#tableWithSearch').DataTable({

        });
    });

</script>
@endsection
