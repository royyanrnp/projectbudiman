<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="token" content="" />

    <title>Register</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    
</head>

<body style="background: #110F12;">

    <div class="background"></div>

    <div class="container hform min-vh-100">
        <div class="card card-content curved-20 border-0 mb-5">
            <div class="card-body px-auto px-md-5">
                <div class="d-flex justify-content-between mt-3 mb-4">
                    <h3 class="font-weight-bold regis-text">Registration</h3>
                </div>

                <form class="form-text" action="{{url('registration')}}" method="POST">
                    @csrf
                    <div class="row mt-5">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Username</label>
                                <input type="text" class="form-control gray" id="username" name="username">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Your Name</label>
                                <input type="text" class="form-control gray" id="name" name="name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control gray" id="email" name="email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control gray" id="phone" name="handphone">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control gray" id="password" name="password">
                            </div>
                        </div>
                        {{-- <div class="col-md-4">
                            <div class="form-group">
                                <label for="confPass">Confirm Password</label>
                                <input type="password" class="form-control gray" id="confPass" name="password">
                            </div>
                        </div> --}}
                    </div>

                    <hr>

                    <!-- Address -->
                    <h3 class="font-weight-bold mid-form-text mt-5 mb-4">Address</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="Province">Province</label>
                                        <select id="province" class="form-control gray" name="id_province">
                                            <option disabled selected>Select Province</option>
                                            @foreach ($provinces as $province)
                                            <option value="{{$province->id}}">{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="District">District</label>
                                        <select id="district" class="form-control gray" name="id_district">
                                            <option selected disabled>Select District</option>
                                            {{-- @foreach ($districts as $district)
                                                <option value="{{$district->id}}">{{$district->name}}</option>                                                
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="City">City</label>
                                        <select id="city" class="form-control gray" name="id_city">
                                            <option selected disabled>Select City</option>
                                            {{-- @foreach ($cities as $city)
                                                <option value="{{$city->id}}">{{$city->name}}</option>                                                
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="Subdistrict">Village</label>
                                        <select id="village" class="form-control gray" name="id_village">
                                            <option selected disabled>Select Village</option>
                                            {{-- @foreach ($villages as $village)
                                                <option value="{{$village->id}}">{{$village->name}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="Address">Address</label>
                                <textarea class="form-control gray" id="Address" rows="4" name="address"></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <hr>

                    <!-- Product -->
                    <h3 class="font-weight-bold bot-form-text mt-5 mb-4">Product</h3>
                    <div class="row product-regist">

                        @foreach ($products as $product)                            
                        <div class="col-md-3">
                            <div class="card curved-15">
                                <div class="card-body p-2">
                                    <label for="option{{$product->id}}"></label>
                                    <input type="radio" name="product" id="option{{$product->id}}" value="{{$product->id}}" class="ceklis" required>
                                    <div class="img">
                                        <img src="{{asset($product->media->url_image)}}" class="curved-10">
                                    </div>
                                    <div class="mt-2">
                                        <p class="font-weight-bold text-product mb-0">{{$product->name}}</p>
                                        <p class="product-desc">{{$product->desc}}</p>
                                    </div>
                                    <div class="d-flex justify-content-between mb-1">
                                        <p class="card-text my-auto">Rp {{$product->price}}</p>
                                        <a href="#" class="btn btn-pill btn-yellow-light">
                                            <small>
                                                <i class="fas fa-search "></i>
                                            </small>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        @endforeach
                            
                            <!-- <div class="col-md-3">
                                <div class="card curved-15">
                                    <div class="card-body">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            @foreach ($products as $product)
                                            <div class="text-center">
                                                <label class="btn btn-secondary ml-3">
                                                    <input type="radio" name="product" id="option{{$product->id}}" value="{{$product->id}}"> 
                                                    <img src="{{asset($product->media->url_image)}}" class="curved-10">
                                                </label>
                                                <h6>{{$product->name}}</h6>
                                                <p>Rp. {{$product->price}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                    </div>

                    <!-- Referal Code -->
                    <div class="row d-flex align-items-center justify-content-between mt-5">
                        <div class="col-md-4 col-8">
                            <div class="form-group">
                                <label for="name">Referal Code</label>
                                <input type="text" class="form-control gray form-control-lg" id="name" name="refferal">
                            </div>
                        </div>
                        
                        <div class="col-md-2 col-4">
                            <label for=""></label>
                            <button class="btn btn-block btn-yellow-light curved-10 py-2">
                                <small>
                                    Checking
                                </small>
                            </button>
                        </div>

                        <div class="col-md-4"></div>
                        
                        <div class="col-md-2 col-12">
                            <label for=""></label>
                            <button type="submit" class="btn btn-register btn-block curved-10 py-2">
                                <small class="font-weight-bold">
                                    Register
                                </small>
                            </button>
                            {{-- <a href="#" type="submit" class="btn btn-register btn-block curved-10 py-2">
                                <small>
                                    Register
                                </small>
                            </a> --}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.map"></script>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

        <script>
        $.noConflict();
        $(document).ready(function(){

            $.get( "../../city/" + $('#province').val(), function( data ) {                    
                populate($('#city'),data);  
                // $("#city").val({{ isset($user->address) ? $user->address->city : '' }});

                $.get( "../../district/" + $('#city').val(), function( data ) {                    
                    populate($('#district'),data);     
                    // $("#district").val({{ isset($user->address) ? $user->address->district : '' }}); 

                    $.get( "../../village/" + $('#district').val(), function( data ) {                    
                        populate($('#village'),data);  
                        // $("#village").val({{  isset($user->address) ? $user->address->subdistrict : '' }});
                    });
                });
            });


            $('#province').on('change',function(){
                
                $('#city').empty();
                $('#district').empty();
                $('#village').empty();

                $.get( "../../city/" + $('#province').val(), function( data ) {
                    populate($('#city'),data); 
                });
            });

            $('#city').on('change',function(){      
                $('#district').empty();
                $('#village').empty();          
                $.get( "../../district/" + $('#city').val(), function( data ) {                    
                    populate($('#district'),data);                      
                });
            });

            $('#district').on('change',function(){    
                $('#village').empty();   
                $.get( "../../village/" + $('#district').val(), function( data ) {                    
                    populate($('#village'),data);                      
                });
            });

            function populate($e,data){
                
                $e.empty();                
                jQuery.each( data, function( i, val ) { 
                    // console.log(val);                                    
                    $e.append(new Option(val['name'], val['id']));                                                           
                });
            }
        });
    </script>
</body>

</html>
