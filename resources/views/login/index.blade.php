<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="token" content="" />

    <title>Register</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">


</head>

<body>

    <div class="background2">
        <div class="container min-vh-100">
            <div class="row justify-content-center vh-100">
                <div class="col-md-5 my-auto">
                    <div class="card curved-20 border-0">
                        <div class="card-body p-5">
                            <div class="d-flex justify-content-between mt-3 mb-4">
                                <h3 class="font-weight-bold regis-text">Login</h3>
                            </div>
            
                            <form class="form-text" action="{{url('/userlogin')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Username or Email</label>
                                    <input type="text" class="form-control gray form-control-lg" id="username" name="username">
                                </div>
            
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control gray form-control-lg" id="password" name="password">
                                </div>
    
                                <button type="submit" class="btn btn-register btn-block curved-10 py-2 mt-5">
                                    <small>
                                        Login
                                    </small>
                                </button>

                                <a href="{{url('/registration')}}">
                                    <p class="mt-2 text-small">Don't have account? Click to Join!</p>
                                </a>
                            </form>
                        </div>
                    </div>
    
                </div>
            </div>
            
        </div>
    </div>

    

</body>

<!-- Optional JavaScript; choose one of the two! -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.map"></script>
<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
