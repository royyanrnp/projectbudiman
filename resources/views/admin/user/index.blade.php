@extends('layouts.master-admin')

@section('content')
<div class="container-fluid mt-3">
    <div class="card curved-10">
        <div class="card-body">
            @if (session('reset_password'))
            <div class="alert alert-success">
                {{ session('reset_password') }}
            </div>
            @endif
            @if (session('admin_user_edit'))
            <div class="alert alert-success">
                {{ session('admin_user_edit') }}
            </div>
            @endif
            @if (session('alert_delete_user'))
            <div class="alert alert-danger">
                {{ session('alert_delete_user') }}
            </div>
            @endif
            <div class="d-flex justify-content-between">
                <h4>Users</h4>

                <form action="{{url('search_user')}}" class="form-inline my-2 my-lg-0" method="POST">
                    @csrf
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="user_keyword">
                <button type="submit" class='icon-black bx bx-search mr-3'></button>
                </form>

            </div>
            <div class="table-responsive table-user mt-4">
                <table class="table table-bordered" id="user-table">
                    <thead>
                        <tr class="">
                            {{-- <th>Uid</th> --}}
                            <th>Username</th>
                            <th>Name</th>
                            <th>Upline</th>
                            <th>Sponsor</th>
                            {{-- <th>No Ktp</th> --}}
                            <th>Handphone</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if ($users->count())
                        @foreach ($users as $user)
                        {{-- {{dd($user)}} --}}
                        <tr>
                            {{-- <td>121251</td> --}}
                            <td>{{$user->username}}</td>
                            <td>{{$user->name}}</td>
                            <td></td>
                            <td></td>
                            {{-- <td>{{$user->no_ktp}}</td> --}}
                            <td>{{$user->handphone}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->address}}, {{$user->village->name}}, {{$user->district->name}},
                                {{$user->city->name}}, {{$user->province->name}} </td>
                            <td>
                                <button class="btn btn-white text-primary" data-target="#edit_user{{$user->id}}"
                                    data-toggle="modal" data-toggle="tooltip" data-placement="bottom" title="Edit User">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button class="btn btn-white text-primary" data-target="#edit_password{{$user->id}}"
                                    data-toggle="modal" data-toggle="tooltip" data-placement="bottom"
                                    title="Change Password">
                                    <i class="fas fa-key"></i>
                                </button>
                                <button class="btn btn-white text-primary" data-target="#add_upline{{$user->id}}"
                                    data-toggle="modal" data-toggle="tooltip" data-placement="bottom"
                                    title="Change Upline">
                                    <i class="fas fa-plus"></i>
                                </button>
                                <button class="btn btn-white text-primary" data-target="#modal_delete{{$user->id}}"
                                    data-toggle="modal" data-toggle="tooltip" data-placement="bottom"
                                    title="Delete User">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        @else
                            <td colspan="7">No Data</td>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@foreach ($users as $user)
<!-- Modal -->
<div class="modal fade" id="edit_user{{$user->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="{{url('user-admin/'.$user->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            @if ($user->media_profile == null)
                            <div class=" text-center">
                                <img src="{{asset($user->media_profile->link ?? 'assets/img/logo.png')}}">
                            </div>
                            @else
                            <div class="profile-img text-center">
                                <img src="{{asset($user->media_profile->link ?? 'assets/img/logo.png')}}">
                            </div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="row">

                        <div class="col-md-4 mb-3">
                            <label for="">Username</label>
                            <input type="text" name="username" id="username" class="form-control gray"
                                value="{{$user->username}}" required>
                        </div>

                        <div class="col-md-4 mb-3">
                            <label for="">Name</label>
                            <input type="text" name="name" id="name" class="form-control gray" value="{{$user->name}}"
                                required>
                        </div>

                        <div class="col-md-4 mb-3">
                            <label for="role">Role</label>
                            <select name="role" class="form-control gray" id="role">
                                <option value="">Admin</option>
                                <option value="">Member</option>
                            </select>
                        </div>

                        <div class="col-md-4 mb-3">
                            <label for="">Nomor KTP</label>
                            <input type="text" name="no_ktp" id="no_ktp" class="form-control gray"
                                value="{{$user->no_ktp}}">
                        </div>

                        <div class="col-md-4 mb-3">
                            <label for="">Nomor Handphone</label>
                            <input type="text" name="handphone" id="handphone" class="form-control gray"
                                value="{{$user->handphone}}" required>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="">Email</label>
                            <input type="email" name="email" id="email" class="form-control gray"
                                value="{{$user->email}}" required>
                        </div>

                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label for="Province">Province</label>
                                <select id="province" class="form-control gray" name="id_province">
                                    <option selected value="{{$user->id_province}}">{{$user->province->name}}</option>
                                    @foreach ($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label for="city">City</label>
                                <select id="city" class="form-control gray" name="id_city" required>
                                    <option selected value="{{$user->id_city}}">{{$user->city->name}}</option>
                                    {{-- @foreach ($cities as $city)
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label for="district">District</label>
                                <select id="district" class="form-control gray" name="id_district" required>
                                    <option selected value="{{$user->id_district}}">{{$user->district->name}}</option>
                                    {{-- @foreach ($districts as $district)
                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label for="village">Village</label>
                                <select id="village" class="form-control gray" name="id_village" required>
                                    <option selected value="{{$user->id_village}}">{{$user->village->name}}</option>
                                    {{-- @foreach ($villages as $village)
                                        <option value="{{$village->id}}">{{$village->name}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label for="">Address</label>
                            <textarea name="address" id="address" cols="30" rows="5" class="form-control gray"
                                required>{{$user->address}}</textarea>
                        </div>

                    </div>

                    <div class="row justify-content-end mt-3">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-block btn-dark curved-20"
                                data-dismiss="modal">Close</button>
                        </div>

                        <div class="col-md-2">
                            <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="edit_password{{$user->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <form action="{{url('reset_password_admin/'.$user->uid)}}" method="POST">
            @csrf
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="username">Username</label>
                            <input class="form-control gray" value="{{$user->username}}" disabled>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="password">New Password</label>
                            <input type="password" name="password" id="password" class="form-control gray">
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Change Password</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach

@foreach ($users as $user)
    {{-- Modal --}}
    <div class="modal fade" id="add_upline{{$user->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="staticBackdropLabel">Settings</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-2 border-right text-center">
                            <div class="nav flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="pl-0 nav-link fas fa-chart-line icon-modal" id="v-upline-tab"
                                    data-toggle="pill" href="#v-upline{{$user->id}}" role="tab" aria-controls="v-upline"
                                    aria-selected="true">
                                </a>

                                <a class="pl-0 nav-link fas fa-user-friends icon-modal" id="v-sponsor-tab"
                                    data-toggle="pill" href="#v-sponsor{{$user->id}}" role="tab" aria-controls="v-sponsor"
                                    aria-selected="false">
                                </a>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="tab-content" id="v-pills-tabContent">
                                {{-- Upline --}}
                                <div class="tab-pane fade show active" id="v-upline{{$user->id}}" role="tabpanel"
                                    aria-labelledby="v-upline-tab">
                                    <h3 class="mb-3">Upline</h3>

                                    <form action="" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="upline">Current Upline</label>
                                                    {{-- {{dd($user)}} --}}
                                                    <input type="text" class="form-control gray" disabled value="{{$user->upline->username ?? '-'}}">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="downline">New Upline</label>
                                                    <input type="text" class="form-control gray" name="new_upline">
                                                </div>
                                            </div>

                                            <div class="col-md-12 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                {{-- Sponsor --}}
                                <div class="tab-pane fade" id="v-sponsor{{$user->id}}" role="tabpanel"
                                    aria-labelledby="v-sponsor-tab">
                                    <h3 class="mb-3">Sponsor</h3>

                                    <form action="" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="upline">Current Sponsor</label>
                                                    {{-- {{dd($user)}} --}}
                                                    <input type="text" class="form-control gray" disabled value="{{$user->sponsor->username ?? '-'}}">
                                                </div>
                                            </div>
        
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="downline">New Sponsor</label>
                                                    <input type="text" class="form-control gray" name="new_sponsor">
                                                </div>
                                            </div>
        
                                            {{-- <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="sponsor">Sponsor</label>
                                                    <input type="text" class="form-control gray">
                                                </div>
                                            </div> --}}
        
                                            <div class="col-md-12 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-primary px-4">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
@endforeach


<!-- Modal -->
<div class="modal fade" id="modal_delete{{$user->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <form action="{{url('user-admin/'.$user->id)}}" method="POST">
            @csrf
            @method('delete')
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to delete this User?</h4>
                    <h5><strong>{{$user->name}}</strong></h5>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-dark curved-20" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger curved-20">Delete</button>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection

@section('script')

<script>
    $(document).ready(function () {

        $('#user-table').DataTable();
    });

</script>
@endsection

@section('additional-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.map"></script>
<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>


<script>
    $.noConflict();
    $(document).ready(function () {

        $.get("../../city/" + $('#province').val(), function (data) {
            populate($('#city'), data);
            $("#city").val({
                {
                    isset(auth() - > user() - > id_city) ? auth() - > user() - > id_city : ''
                }
            });

            $.get("../../district/" + $('#city').val(), function (data) {
                populate($('#district'), data);
                $("#district").val({
                    {
                        isset(auth() - > user() - > id_district) ? auth() - > user() - >
                            id_district : ''
                    }
                });

                $.get("../../village/" + $('#district').val(), function (data) {
                    populate($('#village'), data);
                    $("#village").val({
                        {
                            isset(auth() - > user() - > id_village) ? auth() - >
                                user() - > id_village : ''
                        }
                    });
                });
            });
        });


        $('#province').on('change', function () {

            $('#city').empty();
            $('#district').empty();
            $('#village').empty();

            $.get("../../city/" + $('#province').val(), function (data) {
                populate($('#city'), data);
                $("#shipping_cost").html(data['shipping_cost']);
            });
            $.get("")
        });

        $('#city').on('change', function () {
            $('#district').empty();
            $('#village').empty();
            $.get("../../district/" + $('#city').val(), function (data) {
                populate($('#district'), data);
            });
        });

        $('#district').on('change', function () {
            $('#village').empty();
            $.get("../../village/" + $('#district').val(), function (data) {
                populate($('#village'), data);
            });
        });

        function populate($e, data) {

            $e.empty();
            jQuery.each(data, function (i, val) {
                // console.log(val);                                    
                $e.append(new Option(val['name'], val['id']));
            });
        }
    });

</script>



@endsection
