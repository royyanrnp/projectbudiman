<div class="container">
    <nav class="navbar navbar-light py-3 px-0">

        <div>
            <a href="{{url('/')}}" class="navbar-brand mb-0 h1 font-weight-bold"><strong>BOSS TRAVEL</strong></a>
        </div>

        <div class="d-flex align-items-center">
            <form class="form-inline my-2 my-lg-0 mr-3" action="{{url('product/search')}}" method="POST">
                @csrf
                <div class="input-group"> 
                    <input class="form-control form-control-lg border-0 curved-10" type="text" placeholder="Search" aria-label="Search" name="product_name" style="background-color: #ECEEF5; font-size:small">
                    <div class="input-group-append ml-2">
                        <button class="btn btn-block curved-10" type="button" style="background-color: #ECEEF5;">
                            <i class="bx bx-search"></i>
                        </button>
                    </div>
                    
                </div>
            </form>
    
            <div class="dropdown">
                {{-- <a href="" class='icon-black bx bx-search mr-3'></a> --}}
                
                @if (auth()->user())
                    <a href="{{url('cart')}}" class='icon-black bx bx-cart mr-3' data-toggle="tooltip" data-placement="bottom" title="Cart"></a>
                    
                @else
                    <a href="{{('userlogin')}}" class='icon-black bx bx-cart mr-3' data-toggle="tooltip" data-placement="bottom" title="Cart"></a>
                @endif
    
                <a href="#" class='icon-black bx bx-user' data-toggle="dropdown">
                    
                    {{auth()->user() ? auth()->user()->name : ''}}</a>
                    <div class="dropdown-menu dropdown-menu-right">
                    @if (auth()->user())
                        <a class="dropdown-item" href="{{url('profile/'.encrypt(auth()->user()->id))}}">Profile</a>
                        <a class="dropdown-item" href="{{url('transaction-list/')}}">
                            <div class="d-flex align-items-center">
                                Transaction
                                <i class='bx bx-time-five ml-2'></i>
                            </div>
                        </a>
                        @if (auth()->user()->flag == 0)
                            <a class="dropdown-item" href="{{url('user-admin')}}">Admin</a>
                        @endif
                        <a class="dropdown-item" href="{{url('/logout')}}">
                            <div class="d-flex align-items-center">
                                Logout
                                <i class='bx bx-log-out ml-2'></i>
                            </div>
                        </a>
                    @else
                    <a class="dropdown-item" href="{{url('/userlogin')}}">
                        <div class="d-flex align-items-center">
                            Login
                           <i class='bx bx-log-in ml-2'></i>
                        </div>
                    </a>
                    <a class="dropdown-item" href="{{url('/registration')}}">
                        <div class="d-flex align-items-center">
                            Register
                            <i class='bx bxs-paper-plane ml-1'></i>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
        </div>
        

    </nav>
</div>
