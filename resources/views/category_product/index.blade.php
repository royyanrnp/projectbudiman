@extends('layouts.master-admin')

@section('content')
<div class="container-fluid mt-3">
    <div class="card curved-10">
        <div class="card-body">
            <div class="row justify-content-between mb-3">
                <div class="col-md-3">
                    <h4>Category</h4>
                </div>
                <div class="col-md-2">
                    <a href="" class="btn btn-block btn-lg btn-dark text-small" data-target="#add_category"
                        data-toggle="modal">
                        Add Category Product
                    </a>
                </div>
            </div>

            <section class="table-user">
                <div class="table-responsive">
                    <table class="table table-bordered" id="category-table">
                        <thead class="">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $pro)
                            <tr>
                                <td class="text-left">{{$loop->iteration}}</td>
                                <td class="text-left">{{$pro->name}}</td>
                                <td class="text-left">{{$pro->desc}}</td>
                                <td>
                                    @if ( $pro->active == true)
                                    <span class="badge badge-success">Active</span>
                                    @else
                                    <span class="badge badge-danger">Non Active</span>
                                    @endif
                                </td>
                                <td>

                                    <button class="btn btn-white text-primary" data-toggle="modal"
                                        data-target="#edit_category{{$pro->id}}" data-toggle="tooltip"
                                        data-placement="bottom" title="Edit Category">
                                        <i class="fas fa-edit"></i>
                                    </button>


                                    <button class="btn btn-white text-primary" data-toggle="modal"
                                        data-target="#delete_category{{$pro->id}}" data-toggle="tooltip"
                                        data-placement="bottom" title="Delete Category">
                                        <i class="fas fa-trash"></i>
                                    </button>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>

            <!-- Modal Add Category -->
            <div class="modal fade" id="add_category" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content curved-20">
                        <form action="{{url('category')}}" method="POST">
                            @csrf
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel">Add Category Product</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-2">
                                    <label><strong>Name</strong></label>
                                    <input type="text" class="form-control gray" name="name" id="name">
                                </div>
                                <div class="mb-2">
                                    <label><strong>Description</strong></label>
                                    <textarea name="desc" id="desc" cols="15" rows="5"
                                        class="form-control gray"></textarea>
                                </div>
                                <div class="mb-3">
                                    <label for=""><strong>Status</strong></label><br>
                                    <input type="radio" value="1" name="active" id="active">
                                    <label for="" class="mr-3">Active</label>
                                    <input type="radio" value="0" name="active" id="active">
                                    <label for="">Non Active</label>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Add</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @foreach ($products as $pro)
            <!-- Modal -->
            <div class="modal fade" id="edit_category{{$pro->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content curved-20">
                        <form action="{{url('category/'.$pro->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Category Product</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body container">
                                <div class="mb-3">
                                    <label>Name</label>
                                    <input class="form-control gray" type="text" name="name" id="name"
                                        value="{{$pro->name}}">
                                </div>
                                <div class="mb-3">
                                    <label for="">Description</label>
                                    <textarea name="desc" id="desc" cols="10" rows="5"
                                        class="form-control gray">{{$pro->desc}}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label for=""><strong>Status</strong></label><br>
                                    <input type="radio" value="1" name="active" id="active"
                                        {{$pro->active == true ? 'checked' : ''}}>
                                    <label for="" class="mr-3">Active</label>
                                    <input type="radio" value="0" name="active" id="active"
                                        {{$pro->active == false ? 'checked' : ''}}>
                                    <label for="">Non Active</label>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="delete_category{{$pro->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content curved-20">
                        <div class="modal-header border-0">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Category Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4> Are you sure want to delete this Category</h4><br>
                            <h5>{{$pro->name}}</h5>
                            <div class="row justify-content-end mt-3">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-dark curved-20"
                                        data-dismiss="modal">Close</button>
                                </div>
                                <div class="col-md-3">
                                    <form action="{{url('category/'.$pro->id)}}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-danger curved-20">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


</div>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#category-table').DataTable();
    });

</script>
@endsection
