<x-layouts>

    <div class="container table-cart min-vh-100">

        {{-- Table --}}
        <form action="{{url('transaction')}}" method="POST">
            @csrf
            <table class="table table-bordered mt-5">

                <thead class="thead-dark">
                    <tr class="text-center">
                        <th scope="col">Images</th>
                        <th scope="col">Product</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Total</th>
                        <th scope="col">Remove</th>
                    </tr>
                </thead>

                <tbody class="img-cart">
                    @if ($carts->count() >= 1)
                        @foreach ($carts as $cart)
                        <tr class="text-center">
                            <td>
                                <img src="{{asset($cart->detail_barang->media->url_image)}}" class="curved-10">
                            </td>
                            <td>{{$cart->detail_barang->name}} <input type="text" hidden name="product[]"
                                    value="{{$cart->id_barang}}"></td>
                            <td>
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <div class="d-flex">
                                            @if ($cart->qty > 1)
                                            <a href="{{url('min_cart/'.$cart->id_barang)}}"
                                                class="add-quantity btn-add-qty mr-2">−</a>
                                            @endif
                                            <input class="form-control gray text-center" type="number" min="0"
                                                value="{{$cart->qty}}" id="input" name="qty_product[]">
                                            <a href="{{url('add_cart/'.$cart->id_barang)}}"
                                                class="add-quantity btn-add-qty ml-2">+</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="font-weight-bold">Rp {{$cart->detail_barang->price}}</td>
                            <td class="font-weight-bold">Rp {{$cart->detail_barang->price * $cart->qty}}</td>
                            <td>

                                <a href="{{url('cart_delete/'.$cart->id)}}" class="btn-remove">
                                        <i class='bx bx-x'></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">
                                <h3>No Items in Cart</h3>
                            </td>
                        </tr>
                    @endif


                    {{-- <tr class="text-center">
                    <td>
                        <img src="{{asset('assets/img/produk2.png')}}" class="curved-10">
                    </td>
                    <td>Giorgio Armani Acqua</td>
                    <td>
                        <div class="row justify-content-center">
                            <div class="col-md-4">
                                <div class="d-flex">
                                    <a href="" class="add-quantity btn-add-qty mr-2" id="minus">−</a>
                                    <input class="form-control gray text-center" type="number" min="0" value="0"
                                        id="input" />
                                    <a href="" class="add-quantity btn-add-qty ml-2" id="plus">+</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="font-weight-bold">Rp 425.000</td>
                    <td class="font-weight-bold">Rp 425.000</td>
                    <td>
                        <button class="btn-remove">
                            <i class='bx bx-x'></i>
                        </button>
                    </td>
                    </tr>

                    <tr class="text-center">
                        <td>
                            <img src="{{asset('assets/img/produk3.png')}}" class="curved-10">
                        </td>
                        <td>Giorgio Armani Acqua</td>
                        <td>
                            <div class="row justify-content-center">
                                <div class="col-md-4">
                                    <div class="d-flex">
                                        <a href="" class="add-quantity btn-add-qty mr-2" id="minus">−</a>
                                        <input class="form-control gray text-center" type="number" min="0" value="0"
                                            id="input" />
                                        <a href="" class="add-quantity btn-add-qty ml-2" id="plus">+</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="font-weight-bold">Rp 250.000</td>
                        <td class="font-weight-bold">Rp 250.000</td>
                        <td>
                            <button class="btn-remove">
                                <i class='bx bx-x'></i>
                            </button>
                        </td>
                    </tr> --}}
                </tbody>
            </table>

            <h3 class="font-weight-bold regis-text mt-5 mb-5">Address</h3>



            <input type="text" value="{{auth()->user()->id}}" name="id_cust_fk" hidden>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shipping_name">Reciever Name</label>
                        <input type="text" class="form-control gray" id="shipping_name" name="shipping_name"
                            value="{{auth()->user()->name}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shipping_phone">Phone Number</label>
                        <input type="text" class="form-control gray" id="shipping_phone" name="shipping_phone"
                            value="{{auth()->user()->handphone}}">
                    </div>
                </div>

                <div class="col-md-4"></div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="Province">Province</label>
                                <select id="province" class="form-control gray" name="id_province" required>
                                    <option selected>Select Province</option>
                                    @foreach ($provinces as $province)
                                    <option value="{{$province->id}}"
                                        {{auth()->user()->id_province == $province->id ? 'selected' : ''}}>
                                        {{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="District">District</label>
                                <select id="district" class="form-control gray" name="id_district" required>
                                    <option selected>Select District</option>
                                    {{-- @foreach ($districts as $district)
                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="City">City</label>
                                <select id="city" class="form-control gray" name="id_city" required>
                                    <option selected>Select City</option>
                                    {{-- @foreach ($cities as $city)
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="Subdistrict">Subdistrict</label>
                                <select id="village" class="form-control gray" name="id_village" required>
                                    <option selected>Select Village</option>
                                    {{-- @foreach ($villages as $village)
                                        <option value="{{$village->id}}">{{$village->name}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="Address">Address</label>
                        <textarea class="form-control gray" id="Address" rows="4"
                            name="address">{{auth()->user()->address}}</textarea required>
                    </div>
                </div>
            </div>


            <div class="row mt-5">
                <div class="col-md-4">
                    <h4 class="font-weight-bold"></h4>
                    <div class="form-group">
                        <label for="courier">Courier Delivery</label>
                        <select id="courier" class="form-control gray" name="courier">
                            @foreach ($couriers as $courier)
                                <option value="{{$courier->id}}">{{$courier->name}}</option>
                            @endforeach
                            {{-- <option value="2">JNT</option>
                            <option value="3">Si Cepat</option>
                            <option value="4">Shopee Express</option>
                            <option value="5">GoSend</option> --}}
                        </select>
                    </div>
                </div>

                <div class="col-md-4"></div>

                {{-- @foreach ($carts as $cart) --}}
                <div class="col-md-4">
                    <h4 class="font-weight-bold">Total</h4>

                    <table class="table table-borderless gray-box-total">
                        <tbody>
                            @php
                                $subsidi = $carts->sum(function ($cart) {
                                    // dd($item);
                                    return $cart->detail_barang->subsidi_shipping * $cart->qty;
                                });
                                $cashback = $carts->sum(function ($cart) {
                                    // dd($item);
                                    return $cart->detail_barang->cashback * $cart->qty;
                                });
                                $subTotal = $carts->sum(function ($cart) {
                                    return $cart->detail_barang->price * $cart->qty;
                                });

                                $total = ($subTotal - $cashback) + auth()->user()->city->shipping_cost - $subsidi;
                            @endphp
                            {{-- {{dd(sum($carts->qty))}} --}}
                            <tr>
                                <td>Subtotal</td>
                                <td>:</td>
                                <td class="text-right"> Rp. {{number_format($subTotal)}}</td>
                            </tr>
                            <tr>
                                <td>Cashback</td>
                                <td>:</td>
                                <td class="text-right text-primary"> Rp. {{number_format($cashback)}} <input type="text" hidden value="{{$cashback}}" name="cashback"></td>
                            </tr>
                            <tr>
                                <td>Shipping Cost</td>
                                <td>:</td>
                                <td id="shipping_cost" class="text-right"> Rp. {{number_format(auth()->user()->city->shipping_cost)}} <input  type="text" hidden value="{{auth()->user()->city->shipping_cost}}"name="shipping_cost"></td>
                            </tr>
                            <tr>
                                <td>Subsidi Shipping</td>
                                <td>:</td>
                                <td class="text-right text-primary"> Rp. {{number_format($subsidi)}} <input type="text" hidden value="{{$subsidi}}" name="subsidi"></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>:</td>
                                <td class="text-right"> Rp. {{number_format($total)}} <input type="text" hidden
                                        value="{{$total}}" name="total"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>




            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="row col-md-4 justify-content-end px-0">
                    <div class="col-md-4 px-0 mt-3">
                        @if ($carts->count())
                            <button type="submit" class="btn btn-register btn-block curved-10 py-2">
                                <small>
                                    Order
                                </small>
                            </button>
                        @endif
                    </div>

                </div>
            </div>


        </form>
        {{-- @endforeach --}}
    </div>

@section('additional-script')
    


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.map"></script>

    <script>
        $.noConflict();
        $(document).ready(function(){

            $.get( "../../city/" + $('#province').val(), function( data ) {                    
                populate($('#city'),data);  
                $("#city").val({{ isset(auth()->user()->id_city) ? auth()->user()->id_city : '' }});

                $.get( "../../district/" + $('#city').val(), function( data ) {                    
                    populate($('#district'),data);     
                    $("#district").val({{ isset(auth()->user()->id_district) ? auth()->user()->id_district : '' }}); 

                    $.get( "../../village/" + $('#district').val(), function( data ) {                    
                        populate($('#village'),data);  
                        $("#village").val({{  isset(auth()->user()->id_village) ? auth()->user()->id_village : '' }});
                    });
                });
            });


            $('#province').on('change',function(){
                
                $('#city').empty();
                $('#district').empty();
                $('#village').empty();

                $.get( "../../city/" + $('#province').val(), function( data ) {
                    populate($('#city'),data); 
                    $("#shipping_cost").html(data['shipping_cost']);
                });
                $.get("")
            });

            $('#city').on('change',function(){      
                $('#district').empty();
                $('#village').empty();          
                $.get( "../../district/" + $('#city').val(), function( data ) {                    
                    populate($('#district'),data);                      
                });
            });

            $('#district').on('change',function(){    
                $('#village').empty();   
                $.get( "../../village/" + $('#district').val(), function( data ) {                    
                    populate($('#village'),data);                      
                });
            });

            function populate($e,data){
                
                $e.empty();                
                jQuery.each( data, function( i, val ) { 
                    // console.log(val);                                    
                    $e.append(new Option(val['name'], val['id']));                                                           
                });
            }
        });
    </script>

@endsection
</x-layouts>
