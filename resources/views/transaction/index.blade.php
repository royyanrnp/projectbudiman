@extends('layouts.master-admin')

@section('content')
<div class="container-fluid mt-3">
    <div class="card curved-10">
        <div class="card-body">
            <h4>Transaction</h4>
            @if (session('delete_transaction'))
            <div class="alert alert-danger">
                {{ session('delete_transaction') }}
            </div>
            @endif
            <div class="table-responsive table-user mt-4">
                <table class="table table-bordered" id="user-table">
                    <thead>
                        <tr class="">
                            {{-- <th>Uid</th> --}}
                            <th>CODE TRANS</th>
                            <th>NAME</th>
                            <th>DATE</th>
                            <th>PV</th>
                            <th>PICKUP</th>
                            <th>BILL</th>
                            <th>Action</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($transactions as $transaction)
                        {{-- {{dd($transaction)}} --}}
                        <tr>
                            <td>{{$transaction->code_trans}} -
                                @if ($transaction->status == 'WP')
                                <span class="badge badge-danger">WP</span>
                                @elseif($transaction->status == 'PC')
                                <span class="badge badge-primary">PC</span>
                                @elseif($transaction->status == 'P')
                                <span class="badge badge-warning">P</span>
                                @elseif($transaction->status == 'S')
                                <span class="badge badge-success">S</span>
                                @endif
                            </td>
                            <td>{{$transaction->user->username}} - {{$transaction->user->name}}</td>
                            <td>{{$transaction->transaction_date ?? '' }}</td>
                            <td>{{$transaction->pv_total}}</td>
                            <td>{{$transaction->is_pickup}}</td>
                            <td>{{$transaction->purchase_cost}}</td>
                            {{-- <td>{{$transaction->address}}, {{$user->village->name}}, {{$user->district->name}},
                            {{$user->city->name}}, {{$user->province->name}} </td> --}}
                            <td>
                                <a href="{{url('transaction/'.$transaction->id)}}" class="btn text-primary"
                                    data-toggle="tooltip" data-placement="bottom" title="View">
                                    <i class="far fa-eye"></i>
                                </a>
                                @if ($transaction->status == 'WP' || $transaction->status == 'P')
                                <a href="" class="btn text-primary" data-target="#settlement{{$transaction->id}}"
                                    data-toggle="modal" data-toggle="tooltip" data-placement="bottom"
                                    title="Settlement">
                                    <i class="far fa-check-circle"></i>
                                </a>
                                @endif
                            </td>
                            <td>
                                @if ($transaction->status != 'PC')
                                <a href="" class="btn text-primary" data-target="#delete{{$transaction->id}}"
                                    data-toggle="modal" data-toggle="tooltip" data-placement="bottom"
                                    title="Void Transaction">
                                    <i class="fas fa-trash"></i>
                                </a>
                                @endif
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@foreach ($transactions as $transaction)
<!-- Modal -->
<form action="{{url('settlement/'.$transaction->id)}}" method="POST">
    @csrf
    <div class="modal fade" id="settlement{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalLabel">Settlement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Are you sure this transaction <strong>{{$transaction->code_trans}}</strong> has been settle
                        {{auth()->user()->name}} ?</h5>

                    {{-- {{dd($transaction->payment)}} --}}
                    @if ($transaction->payment)
                    <div class="img">
                        <img src="{{asset($transaction->payment->url_image)}}" class="img-fluid curved-10">
                    </div>
                    @endif

                    <h5 class="mt-5"><strong>Date:</strong></h5>
                    <div>
                        <input class="form-control" type="date" name="transaction_date" id="transaction_date"
                            {{$transaction->payment == null ? 'disabled' : ''}} required>
                    </div>
                    <div class="row justify-content-end mt-3">
                        <div class="col-md-3">
                            <button type="button" class="btn btn-block btn-dark curved-20"
                                data-dismiss="modal">Close</button>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-primary curved-20">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</form>


<form action="{{url('transaction/'.$transaction->id)}}" method="POST">
    @csrf
    @method('delete')
    <div class="modal fade" id="delete{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalLabel">Void Transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="text-justify">Are you sure want to void this transaction ?</h5>
                    <h5><strong>{{$transaction->code_trans}}</strong></h5>
                    <div class="row justify-content-end mt-3">
                        <div class="col-md-3">
                            <button type="button" class="btn btn-block btn-dark curved-20"
                                data-dismiss="modal">Close</button>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-danger curved-20">Delete</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</form>

@endforeach
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#user-table').DataTable();
    });

</script>
@endsection
