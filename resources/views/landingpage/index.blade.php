<x-layouts>

    @if (session('failed_search'))
    <div class="alert alert-warning">
        {{ session('failed_search') }}
    </div>
    @endif
    <div>
        {{-- Coba2 --}}

        {{-- <div id="carouselExampleControls" class="carousel slide container2" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{asset('assets/img/produk1.png')}}" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('assets/img/produk1.png')}}" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('assets/img/produk1.png')}}" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> --}}


        {{-- Home Image --}}
        <div class="container landingpage">
            <div class="jumbotron mt-4">
                <div class="px-5 h-100 d-flex">
                    <div class="my-auto">
                        <p class="text-small text-white font-italic">- Parfum Products</p>
                        <h1 class="font-weight-bold">
                            We Offer the Best <br>
                            Parfume for your Body
                        </h1>
                        <button type="button" class="btn btn-shopnow mt-3">
                            <small>Shop Now</small>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        {{-- The Categories --}}
        <div class="container my-5">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <p class="text-small font-italic">- The Categories</p>
                    <h1 class="font-weight-bold">Browse by Category </h1>
                </div>

                {{-- Category --}}
                <div class="col-md-8">
                    <div class="owl-carousel">
                        <div class="card box-cat border-0 h-100 ">
                            <button onclick="category_click(0)" class="curved-20 border-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            All
                                        </div>
                                    </div>
                                </div>
                            </button>
                        </div>
                        @foreach ($categories as $category)
                        <div class="card box-cat border-0 h-100 ">
                            <button
                                onclick="category_click({{$products->where('category_product', $category->id)->first()->category_product ?? 0}})"
                                class="curved-20 border-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            {{$category->name}}
                                        </div>
                                    </div>
                                </div>
                            </button>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        {{-- Our Products --}}
        <div class="container my-5">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <p class="text-small font-italic">- Our Products</p>
                    <h1 class="font-weight-bold">Explore Our Products</h1>
                </div>

                {{-- Button --}}
                <div class="col-md-6 d-flex justify-content-end mb-3">
                    <button class="bx bx-chevron-left nav-btn-size rounded-circle mr-3" hidden></button>
                    <button class="bx bx-chevron-right nav-btn-size rounded-circle" hidden></button>
                </div>
            </div>

            {{-- Product --}}
            <div class="row product-regist" id="products">
                {{-- {{dd($products)}} --}}
                @if ($products->count() >= 1)
                @foreach ($products as $product)
                    <div class="col-md-3">
                        <a href="{{url('product/'.$product->id)}}" class="text-dark">
                            {{-- <input type="radio" name="card" id="card_one"> --}}
                            <label for="card_one" class="w-100">
                                <div class="card curved-15">
                                    <div class="card-body p-2">
                                        <div class="img">
                                            <img src="{{asset($product->media->url_image)}}" class="curved-10">
                                        </div>
                                        <div class="mt-2">
                                            <p class="font-weight-bold text-product mb-0">{{$product->name}}</p>
                                            <p class="product-desc"> {{$product->tagline ? '- '.$product->tagline.' -' : '' }} </p>
                                        </div>
                                        <div class="d-flex justify-content-between mb-1">
                                            <span class="badge badge-pill badge-yellow d-flex align-items-center justify-content-center">Parfume</span>
                                            <p class="card-text font-weight-bold my-auto">Rp. {{number_format($product->price)}}</p>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </a>
                    </div>
                @endforeach
                @else
                <div class="row">
                    <div class="col-md-12">
                        <span style="background-color: lightgrey; color: red" class="curved-20 p-3 ml-3">There is no
                            product available for now</span>
                    </div>
                </div>
                @endif
            </div>
        </div>

        {{-- <div class="row mt-5 justify-content-center pagination">
            <div class="col-4 align-middle">
                {{ $products->links() }}
    </div>
    </div> --}}


    </div>

    @section('additional-script')
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop: false,
            margin: 15,
            nav: true,
            items: 5,
            autoplay: false,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                200: {
                    items: 2
                },
                400: {
                    items: 3
                },
                600: {
                    items: 4
                },
                800: {
                    items: 5
                },

            }
        })

    </script>

    <script>
        // category_click('1');
        console.log('oke');
        function category_click(category_product) {
            var products = {!!$products!!}

            // console.log(products);

            $('#products').html("");

            var ctr = 1;
            var url = window.location.origin;
            // var asset = '{{asset('$product->media->url_image')}}'
            $.each(products, function (i, v) {
                console.log(v);
                if (v.category_product == category_product) {
                    // alert( "Index #" + i + ": " + v );
                    $('#products').append('\
                <div class="col-md-3">\
                    <a href="' + url + '/product/' + v.id + '">\
                        {{-- <input type="radio" name="card" id="card_one"> --}}\
                        <label for="card_one" class="w-100">\
                            <div class="card curved-15">\
                                <div class="card-body p-2">\
                                    <div class="img">\
                                        <img src="' + url + '/' + v.media.url_image + '" class="curved-10">\
                                    </div>\
                                    <div class="mt-2">\
                                        <p class="font-weight-bold text-product mb-0">' + v.name + '</p>\
                                        <p class="product-desc">' + v.tagline.substring(0, 120) + '... See More</p>\
                                    </div>\
                                    <div class="d-flex justify-content-between mb-1">\
                                        <span class="badge badge-pill badge-yellow d-flex align-items-center justify-content-center">Parfume</span>\
                                        <p class="card-text font-weight-bold my-auto">Rp. ' + v.price.toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</p>\
                                    </div>\
                                </div>\
                            </div>\
                        </label>\
                    </a>\
                </div>'
                    );
                } else if (category_product == 0) {
                    $('#products').append('\
                <div class="col-md-3">\
                    <a href="' + url + '/product/' + v.id + '">\
                        {{-- <input type="radio" name="card" id="card_one"> --}}\
                        <label for="card_one" class="w-100">\
                            <div class="card curved-15">\
                                <div class="card-body p-2">\
                                    <div class="img">\
                                        <img src="' + url + '/' + v.media.url_image + '" class="curved-10">\
                                    </div>\
                                    <div class="mt-2">\
                                        <p class="font-weight-bold text-product mb-0">' + v.name + '</p>\
                                        <p class="product-desc">' + v.tagline.substring(0, 120) + '... See More</p>\
                                    </div>\
                                    <div class="d-flex justify-content-between mb-1">\
                                        <span class="badge badge-pill badge-yellow d-flex align-items-center justify-content-center">Parfume</span>\
                                        <p class="card-text font-weight-bold my-auto">Rp. ' + v.price.toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</p>\
                                    </div>\
                                </div>\
                            </div>\
                        </label>\
                    </a>\
                </div>'
                    );
                }
            });

            $('html, body').animate({
                scrollTop: $("#products").offset().top
            }, 1000);
        }

    </script>
    @endsection

</x-layouts>
