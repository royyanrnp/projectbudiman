<x-layouts>
    <div class="container min-vh-100">

        <h1 class="font-weight-bold my-3">Transaction List</h1>
        <div class="text-center">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="table-responsive table-list">
            <table class="table table-bordered mt-3" id="myTable">
                <thead>
                    <tr class="">
                        <th>Transaction Code</th>
                        <th>Status</th>
                        <th>Order Date</th>
                        <th>Purchase Cost</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{$transaction->code_trans}}</td>
                            <td>
                                @if ($transaction->status == 'WP')
                                    <span class="badge badge-danger">WP</span>    
                                @elseif($transaction->status == 'PC')
                                    <span class="badge badge-primary">PC</span>
                                @elseif($transaction->status == 'P')
                                    <span class="badge badge-warning">P</span>
                                @elseif($transaction->status == 'S')
                                    <span class="badge badge-success">S</span>
                                @endif
                                {{-- <span class="badge badge-success">P</span> --}}
                            </td>
                            <td>{{$transaction->created_at}}</td>
                            <td>{{number_format($transaction->purchase_cost)}}</td>
                            <td>
                                <a href="{{url('transaction_detail/'.$transaction->code_trans)}}"><i class="far fa-eye"></i></a>
                                {{-- <button class="btn btn-primary"></button> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>

    </div>

    @section('additional-script')
 
    <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/boxicons@2.1.1/dist/boxicons.js"></script>
    <script src="//cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script> -->
    <script>
        $(document).ready(function () {
            $('#transaction-list').DataTable();
        });

    </script>
    @endsection
    
</x-layouts>