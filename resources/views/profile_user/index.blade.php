<x-layouts>

    <div class="container min-vh-100 mt-3">
        <div class="">
            <div class="">
                <h4>Profile</h4>
                @if (session('edit_bank'))
                <div class="alert alert-success">
                    {{ session('edit_bank') }}
                </div>
                @endif
                <hr>
                <div class="mt-3">
                    <div class="card border-0 curved-10">
                        <div class="card-body">
                            <div class="row justify-content-between align-items-center px-0 pl-3">
                                <div class="col-md-4 mb-3">

                                </div>
                                <div class="row col-md-4 justify-content-end mr-1">
                                    <a href="" class="btn-edit" data-toggle="modal" data-target="#settings">
                                        <i class='bx bxs-cog'></i>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    @if ($user->media_profile == null)
                                    <div class=" text-center">
                                        <img src="{{asset($user->media_profile->link ?? 'assets/img/logo.png')}}">
                                    </div>
                                    @else
                                    <div class="profile-img text-center">
                                        <img src="{{asset($user->media_profile->link ?? 'assets/img/logo.png')}}">
                                    </div>
                                    @endif
                                </div>

                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <h4 class="font-weight-bold">{{$user->name}}</h4>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="d-flex align-items-center">
                                                <i class='bx bx-line-chart mr-4' data-toggle="tooltip" data-placement="bottom" title="Upline"></i>
                                            </div>

                                            <hr>

                                            <div class="d-flex align-items-center">
                                                <i class='bx bx-id-card mr-4' data-toggle="tooltip" data-placement="bottom" title="NIK"></i>
                                                {{$user->no_ktp ?? '-'}}
                                            </div>

                                            <hr>

                                            <div class="d-flex align-items-center">
                                                <i class='bx bx-envelope mr-4' data-toggle="tooltip" data-placement="bottom" title="Email"></i>
                                                {{$user->email}}
                                            </div>

                                            <hr>

                                            <div class="d-flex align-items-center">
                                                <i class='bx bxs-phone mr-4' data-toggle="tooltip" data-placement="bottom" title="Phone Number"></i>
                                                {{$user->handphone}}
                                            </div>

                                            <hr>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="d-flex align-items-center mb-3">
                                                <i class='bx bxs-group' data-toggle="tooltip" data-placement="bottom" title="Sponsor"></i>
                                            </div>

                                            <hr>

                                            <div class="d-flex align-items-center mb-3">
                                                <i class='bx bxs-bank mr-4' data-toggle="tooltip" data-placement="bottom" title="Bank Account"></i>
                                                {{$user->detail_bank->name ?? ''}} - {{$user->bank_acc_num  ?? ''}}
                                            </div>

                                            <hr>

                                            <div class="d-flex align-items-center mb-3">
                                                <i class='bx bxs-id-card mr-4' data-toggle="tooltip" data-placement="bottom" title="NPWP"></i>
                                                {{$user->no_npwp ?? '-'}}
                                            </div>

                                            <hr>

                                            <div class="d-flex align-items-center mb-3">
                                                <i class='bx bx-home-alt mr-4' data-toggle="tooltip" data-placement="bottom" title="Address"></i>
                                                {{$user->address}}, {{$user->village->name}}, {{$user->district->name}},
                                                {{$user->city->name}}, {{$user->province->name}} - {{$user->kodepos}}
                                            </div>

                                            <hr>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="d-flex align-items-center mb-3">
                                                <i class='bx bx-calendar-check mr-4' data-toggle="tooltip" data-placement="bottom" title="Status"></i>
                                                {{$user->activated_at ?? 'Not active'}}
                                            </div>

                                            <hr>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    <!-- Modal -->
    <div class="modal fade" id="settings" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="staticBackdropLabel">Settings</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-2 border-right text-center">
                            <div class="nav flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="pl-0 nav-link active bx bxs-id-card icon-modal" id="v-profile-tab"
                                    data-toggle="pill" href="#v-profile" role="tab" aria-controls="v-profile"
                                    aria-selected="true">
                                </a>
                                <a class="pl-0 nav-link bx bx-current-location icon-modal" id="v-address-tab"
                                    data-toggle="pill" href="#v-address" role="tab" aria-controls="v-address"
                                    aria-selected="false">
                                </a>
                                <a class="pl-0 nav-link bx bxs-key icon-modal" id="v-password-tab" data-toggle="pill"
                                    href="#v-password" role="tab" aria-controls="v-password" 
                                    aria-selected="false">
                                </a>
                                <a class="pl-0 nav-link bx bxs-credit-card-alt icon-modal" id="v-bank-tab"
                                    data-toggle="pill" href="#v-bank" role="tab" aria-controls="v-bank"
                                    aria-selected="false">
                                </a>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="tab-content" id="v-pills-tabContent">

                                {{-- Profile --}}
                                <div class="tab-pane fade show active" id="v-profile" role="tabpanel"
                                    aria-labelledby="v-profile-tab">
                                    <h3>Profile</h3>

                                    <form action="{{url('edit_profile/'.$user->id)}}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="row mt-3">

                                            {{-- Name --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" class="form-control gray" id="name" name="name"
                                                        value="{{$user->name}}">
                                                </div>
                                            </div>

                                            {{-- Username--}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="username">Username</label>
                                                    <input type="text" class="form-control gray" id="username"
                                                        name="username" value="{{$user->username}}">
                                                </div>
                                            </div>

                                            {{-- Email --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" class="form-control gray" id="email" name="email"
                                                        value="{{$user->email}}">
                                                </div>
                                            </div>

                                            {{-- Phone --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone">Phone</label>
                                                    <input type="text" class="form-control gray" id="handphone"
                                                        name="handphone" value="{{$user->handphone}}">
                                                </div>
                                            </div>

                                            {{-- Gender --}}
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="gender">Gender</label>
                                                    <select class="form-control gray" name="gender" id="gender">

                                                        <option value="male"
                                                            {{$user->gender == 'male' ? 'selected' : ''}}>Male</option>
                                                        <option value="female"
                                                            {{$user->gender == 'female' ? 'selected' : ''}}>Female
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- POB --}}
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tempat_lahir">Place of Birth</label>
                                                    <input type="text" class="form-control gray" id="tempat_lahir"
                                                        name="tempat_lahir" value="{{$user->tempat_lahir}}">
                                                </div>
                                            </div>

                                            {{-- DOB --}}
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tanggal_lahir">Date of Birth</label>
                                                    <input type="date" class="form-control gray" id="tanggal_lahir"
                                                        name="tanggal_lahir" value="{{$user->tanggal_lahir}}">
                                                </div>
                                            </div>


                                            {{-- NIK --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nik">NIK</label>
                                                    <input type="text" class="form-control gray" id="no_ktp"
                                                        name="no_ktp" value="{{$user->no_ktp}}">
                                                </div>
                                            </div>

                                            {{-- NPWP --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="no_npwp">NPWP</label>
                                                    <input type="text" class="form-control gray" id="no_npwp"
                                                        name="no_npwp" value="{{$user->no_npwp}}">
                                                </div>
                                            </div>

                                            {{-- Change Profile Image --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="profile_picture">Change Profile Image</label>
                                                    <input type="file" class="form-control-file" id="profile_picture"
                                                        name="profile_picture">
                                                </div>
                                            </div>

                                            {{-- Change Button --}}
                                            <div class="col-md-12 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-success">Save Changes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                {{-- Address --}}
                                <div class="tab-pane fade" id="v-address" role="tabpanel"
                                    aria-labelledby="v-address-tab">
                                    <h3>Address</h3>

                                    <form action="{{url('edit_address/'.$user->id)}}" method="POST">
                                        @csrf
                                        <div class="row mt-3">
                                            {{-- Province --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Province">Province</label>
                                                    <select id="province" class="form-control gray" name="province">
                                                        <option selected value="{{$user->id_province}}">
                                                            {{$user->province->name}}</option>
                                                        @foreach ($provinces as $province)
                                                        <option value="{{$province->id}}">{{$province->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- City --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="City">City</label>
                                                    <select id="city" class="form-control gray" name="city">
                                                        <option selected value="{{$user->id_city}}">
                                                            {{$user->city->name}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- District --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="District">District</label>
                                                    <select id="district" class="form-control gray" name="district">
                                                        <option selected value="{{$user->id_district}}">
                                                            {{$user->district->name}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- Subdistrict --}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="village">Village</label>
                                                    <select id="village" class="form-control gray" name="village">
                                                        <option selected value="{{$user->id_village}}">
                                                            {{$user->village->name}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- Postal Code --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="kodepos">Postal Code</label>
                                                    <input type="text" class="form-control gray" id="kodepos"
                                                        name="kodepos" value="{{$user->kodepos}}">
                                                </div>
                                            </div>

                                            {{-- Address --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="address">Address</label>
                                                    <textarea class="form-control gray" name="address" id="address"
                                                        cols="30" rows="10">{{$user->address}}</textarea>
                                                </div>
                                            </div>

                                            {{-- Change Button --}}
                                            <div class="col-md-12 d-flex justify-content-end">
                                                <Button type="submit" class="btn btn-success">Save Changes</Button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                {{-- Change Password --}}
                                <div class="tab-pane fade" id="v-password" role="tabpanel"
                                    aria-labelledby="v-password-tab">
                                    <h3>Change Password</h3>

                                    <form action="{{url('reset_password_user')}}" method="POST">
                                        @csrf
                                        <div class="row mt-3">

                                            {{-- New Pass --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="newPass">New Password</label>
                                                    <input type="password" class="form-control gray" id="newPass"
                                                        name="password">
                                                </div>
                                            </div>

                                            {{-- Conf New Pass --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="confNewPass">Confirm New Password</label>
                                                    <input type="password" class="form-control gray" id="confNewPass"
                                                        name="password">
                                                </div>
                                            </div>

                                            {{-- Change Button --}}
                                            <div class="col-md-12 d-flex justify-content-end">
                                                <Button type="submit" class="btn btn-success">Save Changes</Button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                {{-- Bank Account --}}
                                <div class="tab-pane fade" id="v-bank" role="tabpanel" aria-labelledby="v-bank-tab">
                                    <h3>Bank Account</h3>

                                    <form action="{{url('edit_bank/'.$user->id)}}" method="POST">
                                        @csrf
                                        <div class="row mt-3">

                                            {{-- Bank --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="bank_name">Bank</label>
                                                    <select class="form-control gray" name="id_bank_fk" id="bank_name">
                                                        {{-- <option value="{{$user->id_bank_fk}}"
                                                        selected>{{$user->detail_bank->name}}</option> --}}
                                                        @foreach ($banks as $bank)
                                                        <option value="{{$bank->id}}"
                                                            {{$bank->id == $user->id_bank_fk ? 'selected' : ''}}>
                                                            {{$bank->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- Bank Account --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="bank_acc_num">Bank Account</label>
                                                    <input type="text" class="form-control gray" name="bank_acc_num"
                                                        id="bank_acc_num" value="{{$user->bank_acc_num}}">
                                                </div>
                                            </div>

                                            {{-- Name Holder --}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nameHolder">Name</label>
                                                    <input type="text" class="form-control gray" name="bank_acc_name"
                                                        id="bank_acc_name" value="{{$user->bank_acc_name}}" readonly>
                                                </div>
                                            </div>

                                            {{-- Add Account --}}
                                            <div class="col-md-12 d-flex justify-content-end">
                                                <Button type="submit" class="btn btn-success">Add Account</Button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="modal-footer border-0">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Understood</button>
                </div> --}}
            </div>
        </div>
    </div>

    

    @section('additional-script')
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.map"></script>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>


    <script>
        $.noConflict();
        $(document).ready(function(){

            $.get( "../../city/" + $('#province').val(), function( data ) {                    
                populate($('#city'),data);  
                $("#city").val({{ isset(auth()->user()->id_city) ? auth()->user()->id_city : '' }});

                $.get( "../../district/" + $('#city').val(), function( data ) {                    
                    populate($('#district'),data);     
                    $("#district").val({{ isset(auth()->user()->id_district) ? auth()->user()->id_district : '' }}); 

                    $.get( "../../village/" + $('#district').val(), function( data ) {                    
                        populate($('#village'),data);  
                        $("#village").val({{  isset(auth()->user()->id_village) ? auth()->user()->id_village : '' }});
                    });
                });
            });


            $('#province').on('change',function(){
                
                $('#city').empty();
                $('#district').empty();
                $('#village').empty();

                $.get( "../../city/" + $('#province').val(), function( data ) {
                    populate($('#city'),data); 
                    $("#shipping_cost").html(data['shipping_cost']);
                });
                $.get("")
            });

            $('#city').on('change',function(){      
                $('#district').empty();
                $('#village').empty();          
                $.get( "../../district/" + $('#city').val(), function( data ) {                    
                    populate($('#district'),data);                      
                });
            });

            $('#district').on('change',function(){    
                $('#village').empty();   
                $.get( "../../village/" + $('#district').val(), function( data ) {                    
                    populate($('#village'),data);                      
                });
            });

            function populate($e,data){
                
                $e.empty();                
                jQuery.each( data, function( i, val ) { 
                    // console.log(val);                                    
                    $e.append(new Option(val['name'], val['id']));                                                           
                });
            }
        });
    </script>

    <!-- <script>
        $.noConflict();
        $(document).ready(function () {

            $.get("../../city/" + $('#province').val(), function (data) {
                populate($('#city'), data);
                $("#city").val({
                    {
                        isset(auth() - > user() - > id_city) ? auth() - > user() - > id_city :
                            ''
                    }
                });

                $.get("../../district/" + $('#city').val(), function (data) {
                    populate($('#district'), data);
                    $("#district").val({
                        {
                            isset(auth() - > user() - > id_district) ? auth() - >
                                user() - > id_district : ''
                        }
                    });

                    $.get("../../village/" + $('#district').val(), function (data) {
                        populate($('#village'), data);
                        $("#village").val({
                            {
                                isset(auth() - > user() - > id_village) ?
                                    auth() - > user() - > id_village : ''
                            }
                        });
                    });
                });
            });


            $('#province').on('change', function () {

                $('#city').empty();
                $('#district').empty();
                $('#village').empty();

                $.get("../../city/" + $('#province').val(), function (data) {
                    populate($('#city'), data);
                });
            });

            $('#city').on('change', function () {
                $('#district').empty();
                $('#village').empty();
                $.get("../../district/" + $('#city').val(), function (data) {
                    populate($('#district'), data);
                });
            });

            $('#district').on('change', function () {
                $('#village').empty();
                $.get("../../village/" + $('#district').val(), function (data) {
                    populate($('#village'), data);
                });
            });

            function populate($e, data) {

                $e.empty();
                jQuery.each(data, function (i, val) {
                    // console.log(val);                                    
                    $e.append(new Option(val['name'], val['id']));
                });
            }
        });

    </script> -->
    
    @endsection
</x-layouts>
