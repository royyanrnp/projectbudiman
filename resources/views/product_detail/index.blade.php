<x-layouts>
    <div class="container min-vh-100">
        <div class="row mt-5 parfume-desc justify-content-between">
            <div class="col-md-4 mt-3">
                <img src="{{asset($product->media->url_image)}}" class="curved-15">
            </div>
            <div class="col-md-7 my-auto">
                <h1 class="text-product-detail">{{$product->name}}</h1>
                <h2 class="text-product-price font-weight-bold">Rp. {{number_format($product->price)}}</h2>
                <div class="mt-5">
                    {{-- <p class="font-italic">- Vitamin C yang nyaman dan tidak perih di lambung -</p> --}}
                    <p class="text-product-desc">{{$product->desc}}</p>
                </div>
                
                
                <div class="row justify-content-end mt-5">
                    @if (auth()->user())
                    <div class="col-5">
                        <a href="{{url('add_cart/'.$product->id)}}" class="btn py-2 btn-block btn-add-cart">
                            Add to Cart
                        </a>
                    </div>
                    @else
                    <div class="col-5">
                        <a href="{{url('userlogin')}}" class="btn py-2 btn-block btn-add-cart">
                            Add to Cart
                        </a>
                    </div>
      
                    @endif
                </div>
                
            </div>
        </div>
    </div>
</x-layouts>