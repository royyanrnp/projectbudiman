@extends('layouts.master-admin')

@section('content')
<div class="container-fluid mt-3">
    <div class="card curved-10">
        <div class="card-body">

            <div class="row justify-content-between mb-3">
                <div class="col-md-3">
                    <h4>Product Type</h4>
                </div>
                <div class="col-md-2">
                    <a href="" class="btn btn-block btn-lg btn-dark text-small" data-target="#add_type"
                        data-toggle="modal">
                        Add Product Type
                    </a>
                </div>
            </div>

            <section class="table-user">
                <table class="table table-bordered" id="myTable">
                    <thead class="text-center">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($adds as $add)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$add->name}}</td>
                            <td>{{$add->desc}}</td>
                            <td class="">

                                <button class="btn btn-white text-primary" data-toggle="modal"
                                    data-target="#edit_type{{$add->id}}" data-toggle="tooltip" data-placement="bottom"
                                    title="Edit Product Type">
                                    <i class="fas fa-edit"></i>
                                </button>


                                <button class="btn btn-white text-primary" data-toggle="modal"
                                    data-target="#delete_type{{$add->id}}" data-toggle="tooltip" data-placement="bottom"
                                    title="Delete Product Type">
                                    <i class="fas fa-trash"></i>
                                </button>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </section>

            <form action="{{url('type')}}" method="POST">
                @csrf
                <!-- Modal Add Type -->
                <div class="modal fade" id="add_type" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content curved-20">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel">Add Product Type</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-2">
                                    <label>Name</label>
                                    <input type="text" class="form-control gray" name="name" id="name">
                                </div>
                                <div class="mb-2">
                                    <label>Description</label>
                                    <textarea name="desc" id="desc" cols="15" rows="5"
                                        class="form-control gray"></textarea>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            @foreach ($adds as $add)
            <form action="{{url('type/'.$add->id)}}" method="POST">
                @csrf
                @method('PUT')
                <!-- Modal -->
                <div class="modal fade" id="edit_type{{$add->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content curved-20">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Product Type</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body container">
                                <div class="mb-3">
                                    <label>Name</label>
                                    <input class="form-control gray" type="text" name="name" id="name"
                                        value="{{$add->name}}">
                                </div>
                                <div class="mb-3">
                                    <label for="">Description</label>
                                    <textarea name="desc" id="desc" cols="10" rows="5"
                                        class="form-control gray">{{$add->desc}}</textarea>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!-- Modal -->
            <div class="modal fade" id="delete_type{{$add->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content curved-20">
                        <div class="modal-header border-0">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Product Type</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4> Are you sure want to delete this Type</h4><br>
                            <h5>{{$add->name}}</h5>
                            <div class="row justify-content-end mt-3">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-dark curved-20"
                                        data-dismiss="modal">Cancel</button>
                                </div>
                                <div class="col-md-3">
                                    <form action="{{url('type/'.$add->id)}}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-danger curved-20">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


</div>
@endsection
