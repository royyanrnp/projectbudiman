@extends('layouts.master-admin')

@section('content')
<div class="container-fluid mt-3">
    <div class="card curved-10">
        <div class="card-body">
            <div class="row justify-content-between mb-3">
                <div class="col-md-3">
                    <h4>Courier</h4>
                </div>
                <div class="col-md-2">
                    <a href="" class="btn btn-block btn-lg btn-dark text-small" data-target="#add_kurir"
                        data-toggle="modal">
                        Add Courier
                    </a>
                </div>
            </div>
            <div class="text-center">

                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif

                @if (session('edit_status'))
                <div class="alert alert-success">
                    {{ session('edit_status') }}
                </div>
                @endif

                @if (session('delete_status'))
                <div class="alert alert-danger">
                    {{ session('delete_status') }}
                </div>
                @endif
            </div>

            <section class="table-user">
                <div class="table-responsive">
                    <table class="table table-bordered" id="category-table">
                        <thead class="">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($couriers as $courier)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$courier->name}}</td>
                                <td>
                                    @if ($courier->status == true)
                                    <span class="badge badge-success">Active</span>
                                    @else
                                    <span class="badge badge-danger">Non Active</span>
                                    @endif
                                </td>
                                <td>

                                    <button class="btn btn-white text-primary" data-toggle="modal"
                                        data-target="#edit_courier{{$courier->id}}" data-toggle="tooltip"
                                        data-placement="bottom" title="Edit Courier">
                                        <i class="fas fa-edit"></i>
                                    </button>


                                    <button class="btn btn-white text-primary" data-toggle="modal"
                                        data-target="#delete_courier{{$courier->id}}" data-toggle="tooltip"
                                        data-placement="bottom" title="Delete Courier">
                                        <i class="fas fa-trash"></i>
                                    </button>

                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>

            <form action="{{url('kurir')}}" method="POST">
                @csrf
                <!-- Modal -->
                <div class="modal fade" id="add_kurir" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content curved-20">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="staticBackdropLabel">Add Courier</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" class="form-control gray" id="name" name="name" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Status</label>
                                            <select class="form-control gray" name="status" id="status">
                                                <option disabled selected>Choose Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Non Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Cancel</button>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Add
                                            Courier</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            @foreach ($couriers as $courier)
            <form action="{{url('kurir/'.$courier->id)}}" method="POST">
                @method('PUT')
                @csrf
                <!-- Modal -->
                <div class="modal fade" id="edit_courier{{$courier->id}}" data-backdrop="static" data-keyboard="false"
                    tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content curved-20">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="staticBackdropLabel">Edit Courier</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" class="form-control gray" id="name" name="name"
                                                value="{{$courier->name}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Status</label>
                                            <select class="form-control gray" name="status" id="status">
                                                <option value="true" {{$courier->status == TRUE ? 'selected' : ''}}>
                                                    Active</option>
                                                <option value="false" {{$courier->status == FALSE ? 'selected' : ''}}>
                                                    Non Active
                                                </option>
                                                {{-- <option disabled selected>Choose Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Non Active</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Cancel</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-primary curved-20">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>



            <form action="{{url('kurir/'.$courier->id)}}" method="POST">
                @method('delete')
                @csrf
                <!-- Modal -->
                <div class="modal fade" id="delete_courier{{$courier->id}}" data-backdrop="static" data-keyboard="false"
                    tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content curved-20">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="staticBackdropLabel">Delete Courier</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Are you sure want to delete this courier?</label>
                                            <h6><strong>{{$courier->name}}</strong></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end mt-3">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-block btn-dark curved-20"
                                            data-dismiss="modal">Cancel</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-danger curved-20">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @endforeach
        </div>
    </div>
</div>
@endsection
