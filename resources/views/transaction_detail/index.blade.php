<x-layouts>
    <div class="container min-vh-100">

        <div class="row">
            <div class="col-md-5 col-8">
                <h3 class="text-transaction font-weight-bold">Transaction Code</h3>
                <h4 class="text-transaction font-weight-bold">{{$transaction->code_trans}} - <span
                        class="badge badge-info">{{$transaction->status}}</span></h4>
                {{-- <h5 class="text-transaction font-weight-bold">Payment Gateaway (Mandiri)</h5> --}}
            </div>

            <div class="col-md-3 col-4">
                <h4 class="text-transaction font-weight-bold">Customer</h4>
                <h5 class="text-small">{{$transaction->user->username}}
                    <!-- username --> - {{$transaction->user->name}}
                    <!-- name -->
                </h5>
                <h5 class="text-small">{{$transaction->user->handphone}}</h5>
            </div>

            <div class="col-md-4">
                {{-- {{dd($transaction)}} --}}
                <h4 class="text-transaction font-weight-bold">Send To:</h4>
                <h5 class="text-small">{{$transaction->shipping_name}}</h5>
                <h5 class="text-small">{{$transaction->shipping_address}}, {{$transaction->village->name}},
                    {{$transaction->district->name}}, {{$transaction->city->name}}, {{$transaction->province->name}}
                </h5>
                <h5 class="text-small">{{$transaction->shipping_phone}}</h5>
               
            </div>
        </div>
        <div class="row justify-content-between align-items-center mt-5">
            <div class="col-md-8">
                    @if ($transaction->status == 'WP' || $transaction->status == 'P')
                        <div class="alert alert-primary text-center">
                            <h5>Order successfull ! Please complete your payment <br>
                                by clicking pay button bellow, and wait for our staff confirmation !</h5>
                        </div>
                    @endif
            </div>

            <div class="col-md-4">
                <h5 class="font-weight-bold">{{$transaction->detail_courier->name}}</h5>
                <p class="font-weight-bold">No Resi : 12031098260461</p>
            </div>

        </div>

        <table class="table table-bordered mt-5">

            <thead class="thead-dark">
                <tr class="text-center">
                    <th>Order Date</th>
                    <th>Transaction Date</th>
                </tr>
            </thead>

            <tbody>
                <tr class="text-center">
                    <td>{{$transaction->created_at}}</td>
                    <td>{{$transaction->transaction_date ?? '-'}}</td>
                </tr>
            </tbody>

        </table>

        <div class="table-responsive">
            <table class="table table-bordered mt-5">

                <thead class="thead-dark">
                    <tr class="text-center">
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($transaction->detail as $item)
                    <tr class="text-center">
                        <td>{{$item->name}}</td>
                        <td>{{$item->qty}}</td>
                        <td class="text-right">Rp. {{number_format($item->qty * $item->sell_price)}}</td>
                    </tr>

                    @endforeach

                    @php
                    $subsidi = $transaction->detail->sum(function ($item) {
                    // dd($item);
                    return $item->detail_barang->subsidi_shipping * $item->qty;
                    });

                    $cashback = $transaction->detail->sum(function ($item) {
                    // dd($item);
                    return $item->detail_barang->cashback * $item->qty;
                    });

                    $subTotal = $transaction->detail->sum(function ($item) {
                    // dd($item);
                    return $item->sell_price * $item->qty;
                    });
                    $total = ($subTotal - $cashback) + $transaction->city->shipping_cost - $subsidi;
                    @endphp

                    <tr class="text-right">
                        <td colspan="2">Subtotal</td>
                        <td>Rp. {{number_format($subTotal)}} </td>
                    </tr>

                    <tr class="text-right">
                        <td colspan="2">Shipping Cost</td>
                        <td>Rp. {{number_format($transaction->city->shipping_cost)}}</td>
                    </tr>

                    <tr class="text-right text-primary">
                        <td colspan="2">Cashback</td>
                        {{-- {{dd($transaction->detail)}} --}}
                        <td>Rp. {{number_format($cashback)}}</td>
                    </tr>

                    <tr class="text-right text-primary">
                        <td colspan="2">Subsidi Shipping</td>
                        <td>Rp {{number_format($subsidi)}}</td>
                    </tr>
                    <tr class="text-right font-weight-bold">
                        <td colspan="2">Total</td>
                        <td>Rp {{number_format($total)}}</td>
                    </tr>
                </tbody>
            </table>

        </div>



        <div class="row mt-5 justify-content-end">
            <div class="col-md-2 text-right col-4">
                <a href="{{url('transaction-list')}}" class="btn curved-15 py-2 btn-block btn-warning">Back</a>
            </div>

            {{-- {{dd($transaction->status)}} --}}
            @if ($transaction->status == 'WP' || $transaction->status == 'P')
                <div class="col-md-2 text-right col-4">
                    <button class="btn curved-15 py-2 btn-block btn-info" data-toggle="modal"
                        data-target="#transfer">Pay</button>
                </div>
            @endif
        </div>

    </div>

    {{-- Modal --}}
    <div class="modal fade" id="transfer" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content curved-20">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="staticBackdropLabel">Transfer</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                                aria-controls="nav-home" aria-selected="true">Step 1</a>
                            <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                                aria-controls="nav-profile" aria-selected="false">Step 2</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                            aria-labelledby="nav-home-tab">
                            <p class="mt-3">
                                Make a down payment for the Reseller Package to the Bank Account of Boss Perfume
                            </p>

                            {{-- Alert --}}
                            <div class="alert alert-success p-0" role="alert" id="alert_copy" style="display: none; ">
                                <p class="text-center my-2">Copied</p>
                            </div>
                            {{-- Top --}}
                            <div class="text-center my-4">
                                <h5 class="font-weight-bold">Bank Rakyat Indonesia (BRI)</h5>
                                <div class="d-flex justify-content-center">
                                    <input type="text" value="123123123" id="bank_bri" hidden >
                                    <h2 class="font-weight-bold" >
                                        123123123
                                    </h2>
                                    <button class="border-0 bg-white" onclick="copy_bri()">
                                        <i class="far fa-copy icon-copy text-primary"></i>
                                    </button>
                                </div>
                                <h5 class="font-weight-bold">Boss Travel Indonesia</h5>
                            </div>
                            {{-- Middle --}}
                            <div class="row align-items-center">
                                <div class="col-5"><hr class="my-3"></div>
                                <div class="col-2 text-center">{{ __('Or') }}</div>
                                <div class="col-5"><hr class="my-3"></div>
                            </div>

                            
                            {{-- Alert --}}
                            <div class="alert alert-success p-0" role="alert" id="alert_copy" style="display: none; ">
                                <p class="text-center my-2">Copied</p>
                            </div>
                            
                            {{-- Bottom --}}
                            <div class="text-center my-4">
                                <h5 class="font-weight-bold">Bank Central Asia (BCA)</h5>
                                <div class="d-flex justify-content-center">
                                    <input type="text" value="6043584567" id="bank_bca" hidden >
                                    <h2 class="font-weight-bold" >
                                        6043584567
                                    </h2>
                                    <button class="border-0 bg-white" onclick="copy_bca()">
                                        <i class="far fa-copy icon-copy text-primary"></i>
                                    </button>
                                </div>
                                <h5 class="font-weight-bold">Boss Travel Indonesia</h5>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            @if ($transaction->payment)
                                <p class="mt-3">File that you uploaded before</p>
                                <img src="{{asset($transaction->payment->url_image)}}" class="img-fluid curved-10">
                                <hr>
                            @endif
                            <p class="mt-3">Upload your proof payment here</p>
                            {{-- {{dd($transaction->code_trans)}} --}}
                            <form action="{{url('transaction-payment_confirm/'.$transaction->code_trans)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <input type="file" class="form-control-file" id="payment_confirm" name="payment_confirm">
                                </div>

                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary"> Upload</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        function copy_bri() {
            // console.log('test');
            /* Get the text field */
            var copyText = document.getElementById("bank_bri");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);
            document.getElementById('alert_copy').style.display="block";
                
            }

        function copy_bca() {
            // console.log('test');
            /* Get the text field */
            var copyText = document.getElementById("bank_bca");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);
            document.getElementById('alert_copy').style.display="block";
                
            }
    </script>
</x-layouts>
