<!DOCTYPE html>
<html  lang="{{ str_replace('_', '-',  app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="token" content="" />
        
    <title>@yield('title')</title>
    <!-- <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    {{-- <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}"> --}}
    
    <link rel="stylesheet" href="{{asset('AlvaMarvello/css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style-dashboard.css')}}">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

    {{-- Css Adding By Royyam --}}
    {{-- <link rel="stylesheet" href="{{asset('assets/css/Royyan/floating.css')}}"> --}}
    @yield('header')
    
    <link rel="stylesheet" href="{{asset('assets/css/AlvaMarvello/additional.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/AlvaMarvello/init.css')}}">

</head>

<body>
    
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/')}}">
                <img class="img-fluid" src="{{ asset( $corporate['logo']->value ?? '' ) }}" alt="{{$corporate['name']->value ?? ''}}">
            </a>

            <hr class="sidebar-divider my-0">
            
            <div class="sidebar-content">
                {{-- {{dd($menus->where('role_id',auth()->user()->detail->type))}} --}}
                @foreach ($menus->where('role_id',auth()->user()->detail->type) as $menu)
                @if ($menu->detail->parent == null)
                    @if ($menu->childs->count())
                    <li class="nav-item-container">
                        <div class="nav-item {{  request()->is('task*') ? 'active' : '' }}">
                            <a class="nav-link collapsed" href="#task" data-toggle="collapse" data-target="#task">
                                <img src="{{asset('assets/img/icon/new-icon-home.svg')}}" alt="">
                                <span class="ml-2">{{$menu->detail->name}}</span>
                            </a>
                        </div>
                        <div class="collapse" id="task" aria-expanded="true">
                            <ul style="list-style:none;">
                                @foreach ($menu->childs as $child)  
                                    <li class="nav-item-container">
                                        <div class="nav-item {{  request()->is(strtolower($child->path).'*') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{$child->route ? route($child->route) : url($child->path) }}">                                
                                                <img class="icon-sidebar" src="{{asset($child->icon)}}" alt="">                                                                
                                                <span class="ml-2">{{ __($child->name) }}</span>                                
                                                
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @else
                        <li class="nav-item-container">
                            <div class="nav-item {{  request()->is(strtolower($menu->detail->path).'*') ? 'active' : '' }}">
                                <a class="nav-link" href="{{$menu->detail->route ? route($menu->detail->route) : url($menu->detail->path) }}">                                
                                    <img class="icon-sidebar" src="{{asset($menu->detail->icon)}}" alt="">                                                                
                                    <span class="ml-2">{{ __($menu->detail->name) }}</span>                                
                                    
                                </a>
                            </div>
                        </li>
                    @endif
                @endif
                    {{-- @if($menu->childs->count())
                        <li class="nav-item-container">
                            <div class="nav-item {{  request()->is(strtolower($menu->detail->path).'*') ? 'active' : '' }}">
                                <a class="nav-link" href="{{$menu->detail->route ? route($menu->detail->route) : url($menu->detail->path) }}">                                
                                    <img class="icon-sidebar" src="{{asset($menu->detail->icon)}}" alt="">                                                                
                                    <span class="ml-2">{{ __($menu->detail->name) }}</span>                                
                                    
                                </a>
                            </div>
                        </li>
                    @endif --}}
                    {{-- @if ($menu->detail->childs->count())
                        <li class="nav-item-container">
                            <div class="nav-item">
                                <a class="nav-link collapsed" href="#{{ __($menu->detail->code) }}" data-toggle="collapse" data-target="#task">
                                    <img src="{{asset($menu->detail->icon)}}" alt="">
                                    <span class="ml-2">{{ __($menu->detail->name) }}</span>
                                </a>
                            </div>
                            <div class="collapse show" id="{{ $menu->detail->code}}" aria-expanded="true">
                                <ul style="list-style:none;">
                                    @foreach ($menu->detail->childs as $child)
                                        <li class="nav-item-container">
                                            <div class="nav-item {{  request()->is(strtolower($child->path).'*') ? 'active' : '' }}">
                                                <a class="nav-link" href="{{$child->route ? route($child->route) : url($child->path) }}">                                
                                                    <img class="icon-sidebar" src="{{asset($child->icon)}}" alt="">                                                                
                                                    <span class="ml-2">{{ __($child->name) }}</span>                                
                                                    
                                                </a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @else
                    @endif --}}
                @endforeach
                <div class="text-center text-lingkaran">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

                {{-- <div class="character">
                    <div class="card-character">
                        <div class="card-body">
                            <div class="character-img">
                                <img src="{{asset('assets/img/gajah.svg')}}" alt="">
                            </div>
                            <h5>Download Our App</h5>
                            <p>Get everything in yours smart
                                phone from now
                            </p>
                        </div>
                    </div>
                    <div class="lingkaran">
                        <div class="btn-lingkaran">
                            <i class="fas fa-arrow-right"></i>
                        </div>
                    </div>
                </div> --}}
            </div>
        </ul>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <div class="nav-content mx-md-3">
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top nav-main">
                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- Topbar Search -->
                        <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            {{-- <div class="input-group">
                                <input type="text" class="form-control form-search bg-light border-0 small" placeholder="Search for..."
                                    aria-label="Search" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div> --}}
                        </form>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                            <li class="nav-item dropdown no-arrow d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-search fa-fw"></i>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                    aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto w-100 navbar-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control bg-light border-0 small"
                                                placeholder="Search for..." aria-label="Search"
                                                aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fas fa-search fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>

                            <!-- Nav Item - Alerts -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bell fa-fw"></i>
                                    <!-- Counter - Alerts -->
                                    <span class="badge badge-danger badge-counter" id="notification-counter"></span>
                                </a>
                                <!-- Dropdown - Alerts -->
                                <div  class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in curved" aria-labelledby="alertsDropdown">
                                    <div class="p-4">
                                        <div class="d-flex justify-content-between">
                                            <h6 class="font-weight-bold mb-0">Notifications</h6>
                                            <a class="text-right my-auto small text-primary" href="{{ route('notifications.index') }}">Show All</a>
                                        </div>
                                        <hr>
                                        <div class="timeline-right p-0">
                                            <div id="notification-body" class="custom-scrollbar-css">                                                

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            

                            <!-- Nav Item - Messages -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown1" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                                    <i class="fas fa-envelope fa-fw"></i>
                                    <!-- Counter - Messages -->
                                    <span class="badge badge-danger badge-counter" id="message-counter"></span>
                                    </span>
                                </a>
                                <div  class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in curved" aria-labelledby="alertsDropdown">
                                    <div class="p-4">
                                        <div class="d-flex justify-content-between">
                                            <h6 class="font-weight-bold mb-0">Message</h6>
                                            <a class="text-right my-auto small text-primary" href="{{ url('/message') }}">Show All</a>
                                        </div>
                                        <hr>
                                        <div class="timeline-right p-0">
                                            <div id="message-body" class="custom-scrollbar-css">                                                

                                            </div>
                                            <div class="row">
                                                <div class="col-12 mt-3">
                                                    <a href="{{ url('/message/new') }}" class="btn btn-block btn-primary h-auto">New Message</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </li>

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->detail->name ?? 'Demo User'}}</span>
                                    <img class="img-profile rounded-circle"
                                        src="{{ auth()->user()->profile_photo_path ? Storage::url(auth()->user()->profile_photo_path) :  asset('assets/img/avatar.png')}}">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                    aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="{{auth()->user() ? url('/profile/'.encrypt(auth()->user()->username)) : '#'}}">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profile
                                    </a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#forgetPassword">
                                        <i class="fas fa-lock mr-2 text-gray-400"></i>
                                        Change Password
                                    </a>
                                    <a class="dropdown-item" href="{{url('settings')}}">
                                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Settings
                                    </a>
                                    {{-- <a class="dropdown-item" href="#">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Activity Log
                                    </a> --}}
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Log out') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- End of Topbar -->

                <div class="content">
                    @yield('content')
                </div>
                {{-- <a href="https://api.whatsapp.com/send?phone=6281337091510&text=Hi *Acelios* team" class="float" target="_blank">
                    <i class="fab fa-whatsapp my-float"></i>
                </a> --}}
                <div class="adminActions">
                    <input type="checkbox" name="adminToggle" class="adminToggle" />
                    <a class="adminButton icon" href="#!">
                        <i class="fas fa-angle-up"></i><br>
                        <p class="text-small mb-0">More info</p><br>
                    </a>
                    <div class="adminButtons">
                        <div class="col-12 d-flex justify-content-end">
                            <a class="float-student-1 bg-success" href="https://api.whatsapp.com/send?phone=6287753967523&text=Hi *Acelios* team..." target="_blank">
                                <i class="fab fa-whatsapp"></i>
                            </a>
                        </div>
                        @if(\Request::route()->getName() == 'preview_task')
                        <div class="col-12 d-flex justify-content-end">
                            <a class="float-student-1 list-student" target="_blank">
                                <i class="fas fa-user-check"></i>
                            </a>
                        </div>
                        @endif
                        @if (\Request::route()->getName() == 'scoring_task')
                        <div class="col-12 d-flex justify-content-end">
                            <a class="float-student-1 list-student" target="_blank">
                                <i class="far fa-user"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
                {{-- <div class="adminActions">
                    <input type="checkbox" name="adminToggle" class="adminToggle" />
                    <a class="adminButton" href="#!">
                        <i class="fas fa-angle-left"></i>
                    </a>
                    <div class="adminButtons mr-5">
                        <div class="row d-flex justify-content-around">
                            <div class="col-3 float-right">
                                <a class="float-student-1 bg-success" href="https://api.whatsapp.com/send?phone=6287753967523&text=Hi *Acelios* team..." target="_blank">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                            </div>
                            @if (\Request::route()->getName() == 'scoring_task')    
                                <div class="col-3 float-left">
                                    <a class="float-student-1 list-student mr-4" target="_blank">
                                        <i class="far fa-user"></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div> --}}
            </div>

            
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Satu Rasi Digital 2020</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <div class="modal fade" id="forgetPassword" tabindex="-1" aria-labelledby="forgetPasswordLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body edit-class">
                    <div class="d-flex justify-content-between">
                        <h3 class="text-primary font-weight-bold">Forget Password</h3>
                        <button type="button" class="btn btn-link" data-dismiss="modal">
                            <i class="far fa-times-circle"></i>
                        </button>
                    </div>
                    <form action="{{ url('profile/change/password') }}" method="post">
                        @csrf
                        <input type="hidden" name="uid" id="" value="{{auth()->user()->uid}}" hidden>
                        <div id="teacher-select">
                            <div class="row">                                                                
                                <div class="col-md-12 mb-3">
                                    <label>Old Password
                                        @error('old_password')
                                            <span class="text-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                        @enderror
                                    </label>
                                    <div class="input-group" id="show_hide_password">
                                        <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary btn-sm" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mb-3">
                                    <label>New Password
                                        @error('password')
                                            <span class="text-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                        @enderror
                                    </label>
                                    <div class="input-group" id="show_hide_password">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" minlength="8">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary btn-sm" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                
                                <div class="col-md-12 mb-3">
                                    <label>Confirm Password</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Password Confirm" minlength="8">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary btn-sm" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row pt-3 pb-2">
                            <div class="col-md-8"></div>
                            <div class="col-md-2 mb-3">
                                <button type="button" class="btn btn-secondary btn-pill btn-block" data-dismiss="modal"><small>Close</small></button>
                            </div>
                            <div class="col-md-2 mb-3">
                                <button type="submit" class="btn btn-primary h-auto btn-block"><small>Save</small></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('assets/js/jquery.easing.min.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('assets/js/js-dashboard.js')}}"></script>
    <script src="{{asset('assets/js/readMoreJS.min.js')}}"></script>
    <script src="{{asset('assets/js/Chart.min.js')}}"></script>
    <script src="{{asset('assets/js/chart-bar-demo.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="{{asset('AlvaMarvello/js/moment.min.js') }}"></script>
    <script src="{{asset('AlvaMarvello/js/bootstrap-datetimepicker.min.js')}}"></script>
    
    <script>
        $(document).ready(function() {
            $("#show_hide_password button").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                    $('#show_hide_password i').removeClass( "fa-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password i').addClass( "fa-eye" );
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).on('click', 'ul .nav-link', function() {
            $(this).addClass('active').removeClass('active');
        });
    </script>
    <script>
		$readMoreJS.init({
			target: '.timeline-right .timeline-content .timeline-desc p',
			numOfWords: 20,
			toggle: true,
			moreLink: 'read more',
			lessLink: 'read less'
		});
    </script>        
   
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

    @yield('declare-scripts')
    @yield('scripts')
    
    <script src="{{ asset('AlvaMarvello/js/helper.js') }}"></script>
    <script src="{{ asset('AlvaMarvello/js/notification.js') }}"></script>
    <script src="{{ asset('AlvaMarvello/js/message.js') }}"></script>   

    @include('notifications.alert')

    <script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js"></script>
    
    <script src="{{asset('AlvaMarvello/js/firebase.js')}}"></script>

    <script>window.MathJax = { MathML: { extensions: ["mml3.js", "content-mathml.js"]}};</script>
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=MML_HTMLorMML"></script>
</body>
</html>