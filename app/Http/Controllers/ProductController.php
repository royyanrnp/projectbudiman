<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Product;
use App\Models\CategoryProduct;
use App\Models\MediaProduct;
use App\Models\TypeProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::get();
        $categories=CategoryProduct::get();
        $types=TypeProduct::get();
        
        
        return view('product.index', compact('products', 'categories', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // dd($request->all());
        $code='P'.Carbon::parse(now())->format('Ymd').rand(100,999);
        // dd($code);
        
        $create=Product::create([
            'code'=>$code,
            'name'=>$request->name,
            'category_product'=>$request->category_product,
            'type_product'=>$request->type_product,
            'price'=>$request->price,
            'gen_1'=>$request->gen_1,
            'gen_2'=>$request->gen_2,
            'gen_3'=>$request->gen_3,
            'desc'=>$request->desc,
            // 'image' => $filename,
            'is_register'=>$request->is_register,
            'status'=>$request->status,

            // 'flag'=>$request->name
        ]);
        
        $filename = $request->url_image->getClientOriginalName();
        $image = $request->url_image->move(public_path('img/product/'), $filename);
        // dd($filename);
        
        $image = MediaProduct::create([
            'id_product' => $create->id,
            'url_image' => 'img/product/'.$filename
        ]);

        // if($request->url_image){
        //     $store_path = $request->url_image->store('public/assets/img');
        //     $image_url = Str::of($store_path)->remove('public/');
            
        //     $uploadImg=MediaProduct::create([
        //         'id_product'=>$create->id,
        //         'url_image'=>$image_url
        //     ]);
        // }
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // dd($id);
        $product = Product::where('id', $id)->first();
        return view('product_detail/index', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Product::where('id',$id)->first();
        $image = MediaProduct::where('id_product', $id)->first();
        // dd($edit->id);
        
        $edit->name = $request->name; 
        $edit->category_product = $request->category_product; 
        $edit->type_product = $request->type_product; 
        $edit->price = $request->price; 
        $edit->gen_1 = $request->gen_1; 
        $edit->gen_2 = $request->gen_2; 
        $edit->gen_3 = $request->gen_3; 
        $edit->status = $request->status; 
        $edit->desc = $request->desc;
        // dd($edit);
        $edit->save();

        if($request->url_image){
            $filename = $request->url_image->getClientOriginalName();
            
            if($image){
                // dd(True);
                $image_save = $request->url_image->move(public_path('img/product'), $filename);
                $image->url_image = 'img/product/'.$filename;
                // dd($image->url_image);
                $image->save();
            }
            else{
                // dd($filename);
                $image = $request->url_image->move(public_path('img/product'), $filename);
                $image_create = MediaProduct::create([
                    'id_product' => $id,
                    'url_image' => 'img/product/'.$filename
                ]);
            }
        }
        
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Product::where('id', $id)->first();
        $delete->delete();
        return back();
    }

    public function add_product(){
        $categories=CategoryProduct::get();
        $types=TypeProduct::get();

        return view('product.create',compact('categories','types'));
    }


    public function search(Request $request){
        // $product = Product::where('name' $r)

        if ($request->product_name) {
            // Product::whereRaw("UPPER('{$name}') LIKE '%'". strtoupper($value)."'%'");?
            $product = Product::where('name', 'ILIKE',"%".strtoupper($request->product_name)."%")->first();
            // dd($product);
            if($product){
                return redirect('product/'.$product->id);
                
            }else{
                return redirect('/')->with('failed_search', 'Sorry we do not find what you looking for' );
            }
        }else{
            return redirect('/')->with('failed_search', 'Sorry we do not find what you looking for' );
        }


    }
}
