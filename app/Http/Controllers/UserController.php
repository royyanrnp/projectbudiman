<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cart;
use App\Models\Province;
use App\Models\Bank;
use App\Models\MediaProfile;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $provinces = Province::all();
        return view('admin\user.index', compact('users', 'provinces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());
        $user = User::where('id',$id)->first();
        
        $user->username = $request->username; 
        $user->name = $request->name; 
        $user->no_ktp = $request->no_ktp; 
        $user->handphone = $request->handphone; 
        $user->email = $request->email; 
        $user->id_village = $request->id_village; 
        $user->id_district = $request->id_district; 
        $user->id_city = $request->id_city; 
        $user->id_province = $request->id_province; 
        $user->address = $request->address; 
        // $user->desc = $request->desc;
        // dd($user);
        $user->save();
        return back()->with('admin_user_edit', 'Successfully changed user data for User:'.$user->username);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id',$id)->first();
        
        

        $destroy = Cart::where('owner', $user->uid);
        $destroy -> delete();

        $destroy = User::where('id', $id);
        $destroy -> delete();

        return back()->with('alert_delete_user', 'Successfully delete user: '. $user->username);
    }

    public function profile($id){

        $user = User::where('id', decrypt($id))->first();
        // dd($user);
        $provinces = Province::all();
        $banks = Bank::all();
        return view('profile_user.index', compact('user', 'provinces', 'banks'));
    }

    public function edit_profile(Request $request, $id)
    {
        //
        $edit = User::where('id',$id)->first();
        // dd($edit->id);
        
        $edit->username = $request->username; 
        $edit->name = $request->name; 
        $edit->no_ktp = $request->no_ktp; 
        $edit->handphone = $request->handphone; 
        $edit->email = $request->email; 
        $edit->gender = $request->gender; 
        $edit->tempat_lahir = $request->tempat_lahir; 
        $edit->tanggal_lahir = $request->tanggal_lahir; 
        $edit->no_npwp = $request->no_npwp; 
        // $edit->id_village = $request->village; 
        // $edit->id_district = $request->district; 
        // $edit->id_city = $request->city; 
        // $edit->id_province = $request->province; 
        // $edit->address = $request->address; 
        // $edit->desc = $request->desc;
        // dd($edit);

        
        if($request->profile_picture){

            if($edit->media_profile){
                // dd($edit);
                $delete = MediaProfile::where('uid', $edit->uid)->first();
                $delete->delete();
                // $delete->save();
            }

            $filename = $request->profile_picture->getClientOriginalName();
            $image = $request->profile_picture->move(public_path('img/profile/'), $filename);
            // dd($filename);
            
            $image = MediaProfile::create([
                'uid' => $edit->uid,
                'link' => 'img/profile/'.$filename
            ]);

        }



        $edit->save();
        return back();
    }


    public function edit_address(Request $request, $id)
    {
        //
        $edit = User::where('id',$id)->first();
        // dd($edit->id);
        
        $edit->id_village = $request->village; 
        $edit->id_district = $request->district; 
        $edit->id_city = $request->city; 
        $edit->id_province = $request->province; 
        $edit->address = $request->address;
        $edit->kodepos = $request->kodepos;
        // dd($edit);
        $edit->save();
        return back();
    }

    public function edit_bank(Request $request){
        $user = auth()->user();

        $user->id_bank_fk = $request->id_bank_fk;
        $user->bank_acc_num  = $request->bank_acc_num;
        $user->bank_acc_name  = $request->bank_acc_name;
        // dd($request->all());
        $user->save();
        return back()->with('edit_bank', 'Successfully edit bank info');
        
    }

    public function reset_password_user(Request $request){
        $user = auth()->user();

        $password = encrypt($request->password);
        $user->password = $password;
        $user->save();

        return back()->with('status', 'Reset password success');
    }

    public function reset_password_admin(Request $request, $uid){
        $user = User::where('uid', $uid)->first();

        $password = encrypt($request->password);
        $user->password = $password;
        $user->save();

        return back()->with('reset_password', 'Reset password success for user:'.$user->name);
    }


    public function search_user(Request $request){
        if($request->user_keyword){
            $users = User::where('username', 'ILIKE', "%".strtoupper($request->user_keyword)."%")->orWhere('name', 'ILIKE', "%".strtoupper($request->user_keyword)."%")->orWhere('email', 'ILIKE', "%".strtoupper($request->user_keyword)."%")->get();
            // dd($users);

            $provinces = Province::all();
            return view('admin\user.index', compact('users', 'provinces'));

        }else{ 
           return redirect('/user-admin');
        }

    }
}
