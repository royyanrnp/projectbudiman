<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\City;
use App\Models\Village;
use App\Models\District;
use App\Models\User;
use App\Models\Product;
use App\Models\Cart;
use Illuminate\Support\Str;

use Auth;



class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::all();
        $products = Product::where('is_register', true)->get();
        return view('register.index', compact('provinces', 'products'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pass = $request->password;
        $uuid = (string) Str::uuid();
        // dd($uuid);
        // dd($request->all());
        $createduser=User::create([
            'uid' => $uuid,
            'username'=>$request->username,
            'name' => $request->name,
            'email' =>$request->email,
            'handphone' => $request->handphone,
            'id_province' => $request->id_province,
            'id_district' => $request->id_district,
            'id_city' => $request->id_city,
            'id_village' => $request->id_village,
            'address' => $request->address,
            // 'refferal' => $request->refferal,
            'password' => encrypt($pass),
            'flag' => 1,
            'status' => false,
            'activated_at'=> null
        ]);

        Auth::loginUsingId($createduser->id);
        
        $addcart=Cart::create([
            'owner'=>auth()->user()->uid,
            'id_barang'=>$request->product,
            'qty'=>1
        ]);

        // $data = User::create($request->except(['_token', 'submit', 'day','month','year', 'confirm_password' ,'dayInWeek']));
        // dd($createduser);
        // dd(auth()->user()->name);

        return redirect('/cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
