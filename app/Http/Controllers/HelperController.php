<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\City;
use App\Models\District;
use App\Models\Village;

class HelperController extends Controller
{
    public function cityByProvince($id){
        $objects = City::where('province_id',$id)->get();
        return $objects;
    }

    public function districtByCity($id){
        $objects = District::where('city_id',$id)->get();
        return $objects;
    }

    public function villageByDistrict($id){
        $objects = Village::where('district_id',$id)->get();
        return $objects;
    }
}
