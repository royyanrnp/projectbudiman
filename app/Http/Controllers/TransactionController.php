<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\User;
use App\Models\Cart;
use App\Models\Product;
use App\Models\TransactionDetail;
use App\Models\PaymentConfirm;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transactions = Transaction::all();
        $users = User::all();
        return view('transaction.index', compact('transactions', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        // $randomAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = strtoupper(Str::random(6));
        // dd($request->all());
        $transaction = Transaction::create([
            'id_cust_fk' => $request->id_cust_fk,
            'code_trans' => $code,
            'shipping_cost' => $request->shipping_cost,
            'shipping_name' => $request->shipping_name,
            'shipping_phone' => $request->shipping_phone,
            'shipping_address' => $request->address,
            'shipping_village' => $request->id_village,
            'shipping_district' =>$request->id_district,
            'shipping_city' => $request->id_city,
            'shipping_province' => $request->id_province,
            'purchase_cost' => $request->total,
            'status' => 'WP',
            'transaction_date' => null,
            'created_userid' => auth()->user()->id,
            'updated_userid' => auth()->user()->id,
            'pv_total' => null,
            'bv_total' => null,
            'is_pickup' => null,
            'subsidi_shipping' => $request->subsidi,
            'courier' => $request->courier

        ]);

        foreach($request->product as $key => $pro){
            $product = Product::where('id', $pro)->first();
            // dd($pro);
            $qty = $request->qty_product[$key];
            $transaction_detail = TransactionDetail::create([
                'id_trans_fk' => $transaction->id,
                'id_barang_fk'=> $product->id,
                'name' => $product->name,
                'qty' => $qty,
                'base_price' => $product->price,
                'sell_price' => $product->price,
                'weight_kg' => 0,
                'cashback' => 0,

            ]);
        }

        $delete_cart = Cart::where('owner', auth()->user()->uid)->get();
        foreach($delete_cart as $del){
            $del->delete();
        }
      
        // foreach($delete as $del){
        //     dd($del);
        //     $del->delete();

        // }
        return redirect('transaction_detail/'.$code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
        // dd($transaction);

        
        $transaction = Transaction::where('id', $transaction->id)->first();
        return view('transaction_detail.index', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // dd($id);
        $delete = Transaction::where('id', $id)->first();
        
        $details = TransactionDetail::where('id_trans_fk', $delete->id)->get();

        foreach($details as $detail){
            $detail->delete();
        }

        $delete->delete();
        return back()->with('delete_transaction', 'Delete transaction successfully');
    }

    public function settlement(Request $request, $id){
        // dd($id);
        
        $settle = Transaction::where('id', $id)->first();
        $settle->transaction_date = $request->transaction_date;
        $settle->status = 'PC';
        
        $customer = User::where('id', $settle->id_cust_fk)->first();
        if($customer->activated_at == null){
            $customer->activated_at = now();
            $customer->flag = 1;
            $customer->status = true;

            $customer->save();
        }
        
        $settle->save();

        return back();
    }

    public function detail($code_trans){
        $transaction = Transaction::where('code_trans', $code_trans)->first();
        return view('transaction_detail.index', compact('transaction'));
    }


    public function payment_confirm(Request $request, $code_trans){
        $user = auth()->user();
        // dd($request->all());
        $filename = $request->payment_confirm->getClientOriginalName();
        // dd($filename);
        $image = $request->payment_confirm->move(public_path('img/payment/'), $filename);

        $payment = PaymentConfirm::updateOrCreate(
            ['owner' => $user->uid,
            'code_trans' => $code_trans],
            [ 'url_image' => 'img/payment/'.$filename]);
        
        return redirect('transaction-list')->with('status', 'Payment complete, please wait until our staff confirm your payment !!');
    }


    // public function delete_cart($owner){
    //     $delete = Cart::where('owner', $owner)->get();
    //     $delete->forget();
    //     return;
    // }
}
