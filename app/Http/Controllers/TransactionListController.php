<?php

namespace App\Http\Controllers;

use App\Models\TransactionList;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = auth()->user();
        $transactions = Transaction::where('id_cust_fk', $user->id)->get();
        // dd($transactions);
        return view('transaction_list/index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransactionList  $transactionList
     * @return \Illuminate\Http\Response
     */
    public function show(TransactionList $transactionList)
    {
        dd($transactionList);
        // $user = auth()->user();
        $transactions = Transaction::where('id_cust_fk', $transactionList)->get();
        // dd($transactions);
        return view('transaction_list/index', compact('transactions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransactionList  $transactionList
     * @return \Illuminate\Http\Response
     */
    public function edit(TransactionList $transactionList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TransactionList  $transactionList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionList $transactionList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransactionList  $transactionList
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionList $transactionList)
    {
        //
    }
}
