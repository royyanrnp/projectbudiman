<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Province;
use App\Models\Product;
use App\Models\Courier;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user=auth()->user();
        // dd($user);
        $carts=Cart::where('owner',$user->uid)->get();
        $provinces = Province::all();
        $products = Product::all();
        $couriers = Courier::where('status', true)->get();

        return view('cart.index',compact('carts','user', 'provinces', 'products', 'couriers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Cart::where('id', $id)->first();
        $delete->delete();
        return back();
    }


    public function add_product($id)
    {

        $user=auth()->user();

        $add=Cart::where([['owner',$user->uid],['id_barang',$id]])->first();

        if(!$add){
            $create = Cart::create([
                'owner' => $user->uid,
                'id_barang' => $id,
                'qty' => 1
            ]);
        }else{
            $add->qty = $add->qty + 1;
            $add->save();
        }
        $carts=Cart::where('owner',$user->uid)->get();

        return redirect('cart');
    }

    public function min_product($id)
    {

        $user=auth()->user();

        $add=Cart::where([['owner',$user->uid],['id_barang',$id]])->first();
        // dd($add->qty - 1);
        $add->qty = $add->qty - 1;
        $add->save();


        $carts=Cart::where('owner',$user->uid)->get();

        return redirect('cart');
    }
}
