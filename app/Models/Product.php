<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function type(){
        return $this -> hasOne(TypeProduct::class, 'id', 'type_product');
    }

    public function category(){
        return $this -> hasOne(CategoryProduct::class, 'id', 'category_product');
    }

    public function media(){
        return $this -> hasOne(MediaProduct::class, 'id_product', 'id');
    }
}
