<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentConfirm extends Model
{
    use HasFactory;
    protected $guarded=[];
}
