<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    // ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function village(){
        return $this -> hasOne(Village::class, 'id', 'id_village');
    }
    public function district(){
        return $this -> hasOne(District::class, 'id', 'id_district');
    }
    public function city(){
        return $this -> hasOne(City::class, 'id', 'id_city');
    }
    public function province(){
        return $this -> hasOne(Province::class, 'id', 'id_province');
    }
    public function detail_bank(){
        return $this -> hasOne(Bank::class, 'id', 'id_bank_fk');
    }
    public function media_profile(){
        return $this -> hasOne(MediaProfile::class, 'uid', 'uid');
    }
    public function upline(){
        return $this -> hasOne(User::class, 'id', 'id_upline_fk');
    }
    public function sponsor(){
        return $this -> hasOne(User::class, 'id', 'id_sponsor_fk');
    }

    // public function type(){
    //     return $this -> hasOne(TypeProduct::class, 'id', 'type_product');
    // }
}
