<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionDetail extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $guarded = [];

    public function detail_barang(){
        return $this -> hasOne(Product::class, 'id', 'id_barang_fk');
    }
}
