<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $guarded =[

    ];

    public function detail_barang(){
        return $this->hasOne(Product::class,'id','id_barang');
    }
}
