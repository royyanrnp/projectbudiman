<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $guarded =[] ;


    public function user(){
        return $this -> hasOne(User::class, 'id', 'id_cust_fk');
    }

    public function detail(){
        return $this -> hasMany(TransactionDetail::class, 'id_trans_fk', 'id');
    }

    public function payment(){
        return $this -> hasOne(PaymentConfirm::class, 'code_trans', 'code_trans');
    }

    public function village(){
        return $this -> hasOne(Village::class, 'id', 'shipping_village');
    }
    public function district(){
        return $this -> hasOne(District::class, 'id', 'shipping_district');
    }
    public function city(){
        return $this -> hasOne(City::class, 'id', 'shipping_city');
    }
    public function province(){
        return $this -> hasOne(Province::class, 'id', 'shipping_province');
    }
    public function detail_courier(){
        return $this -> hasOne(Courier::class, 'id', 'courier');
    }

    // public function media(){
    //     return $this -> hasOne(MediaProduct::class, 'id_product', 'id');
    // }
}
